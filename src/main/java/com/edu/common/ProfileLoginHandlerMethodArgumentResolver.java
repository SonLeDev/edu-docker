package com.edu.common;

import com.edu.domain.ProfileSignIn;
import com.edu.exceptions.LoginExceptions;
import com.edu.exceptions.RuntimeEduException;
import com.edu.form.ProfileLoginArg;
import com.edu.repository.ProfileSignInRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by sstvn on 5/2/17.
 */
@Component
public class ProfileLoginHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {


    @Autowired
    ProfileSignInRepository profileSignInRepository;

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterType().equals(ProfileLoginArg.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter,
                                  ModelAndViewContainer modelAndViewContainer,
                                  NativeWebRequest nativeWebRequest,
                                  WebDataBinderFactory webDataBinderFactory) throws Exception {

        String userId = nativeWebRequest.getHeader("userId");
        String accessToken = nativeWebRequest.getHeader("accessToken");
        String deviceId = nativeWebRequest.getHeader("deviceId");

// header process
        HttpServletRequest request = (HttpServletRequest) nativeWebRequest
                .getNativeRequest();
        String ip = getRemoteIpAddress(request);

        ProfileSignIn profileSignIn = profileSignInRepository.findByAccessToken(accessToken);

        if (profileSignIn == null) {
            //LOGGER.info("####################################         2");

            throw RuntimeEduException.INVALID_HEADER;
        }

        return new ProfileLoginArg(profileSignIn.getProfileId(),profileSignIn.getDeviceId(),accessToken);
    }
    private String getRemoteIpAddress(HttpServletRequest req) {
        String ip = req.getHeader("X-FORWARDED-FOR");
        if (ip == null) {
            ip = req.getRemoteAddr();
        }
        return ip;
    }
}
