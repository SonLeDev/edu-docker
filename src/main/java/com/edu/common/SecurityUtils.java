package com.edu.common;

import java.util.Random;

/**
 * Created by sstvn on 5/1/17.
 */
public class SecurityUtils {
    public static final String GIVEN_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    public static String generateToken(){

        StringBuilder secureStr = new StringBuilder();
        Random rnd = new Random();
        while (secureStr.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * GIVEN_CHARS.length());
            secureStr.append(GIVEN_CHARS.charAt(index));
        }
        String tokenStr = secureStr.toString();
        // Base64.encodeBase64String( salt );
        return tokenStr;
    }



}
