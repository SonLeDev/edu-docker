package com.edu.common;

/**
 * Created by ChangWon.Son on 14. 11. 12..
 */
public enum ResponseType {
    /**
     * 성공
     */
    SUCCESS(true, "SUCCESS", 0),
    CUSTOM_ERROR(false, "custom message", -4099),
    /**
     * 토큰이나 페이코아이디 값이 전송되지 않았을 경우
     */
    REQUIRED_TOKEN(false, "ไม่ได้ล็อกอินปกติ กรุณาลองล็อกอินใหม่อีกครั้งค่ะ.", -1000),
    /**
     * EMPTY_SNS_TOKEN
     */
    EMPTY_SNS_TOKEN(false, "EMPTY_SNS_TOKEN.", -1000),
    /**
     * 코미코 회원가입이 되어있지 않은 경우
     */
    NOT_REGISTER(false, "NOT_REGISTER", -1001),
    /**
     * 코미코 회원가입이 되어 있는 경우
     */
    ALREADY_REGISTER(false, "ALREADY_REGISTER", -1002),
    /**
     * 지원하지 않는 결재 수단일 경우
     */
    PURCHASE_INVALID_METHOD(false, "등록되지 않은 열람방법입니다.", -6034),
    /**
     * 닉네임이 적절하지 않은 경우
     */
    INVALID_NICKNAME(false, "กรุณาใส่ชื่อเล่นใหม่ค่ะ ไม่สามารถใช้คำหยาบคายได้และไม่เกิน 15 ตัว(ไทย/อังกฤษ).", -1003),
    /**
     * 토큰이 유효하지 않은 경우
     */
    INVALID_DUPLICATE_NICKNAME(false, "INVALID_DUPLICATE_NICKNAME.", -1007),
    INVALID_INVALIDWORD_NICKNAME(false, "CONTAINS_BANNED_WORD.", -1008),
    INVALID_TOKEN(false, "INVALID_TOKEN", -1004),
    NEOID_LOGIN_ERROR(false, "NEOID_LOGIN_ERROR", -9904),
    INVALID_OPENID_TOKEN(false, "INVALID_OPENID_TOKEN", -1004),
    /**
     * 헤더가 유효하지 않은 경우
     */
    INVALID_HEADER(false, "INVALID_HEADER", -1005),

    /**
     * 파라미터가 부적절 할 경우
     */
    INVALID_PARAMETER(false, "INVALID_PARAMETER", -9000),
    /**
     * 댓글 중복 좋아요
     */
    ALREADY_LIKE_COMMENT(false, "เป็นข้อความที่ท่านแนะนำไปแล้วค่ะ.", -4001),
    /**
     * 댓글 좋아요 실패
     */
    FAIL_LIKE_COMMENT(false, "FAIL_LIKE_COMMENT", -4002),
    /**
     * 자신에게 Good 평가
     */
    LIKE_SELF(false, "ไม่สามรถกดแนะนำข้อความที่ตนเขียนเองค่ะ.", -4003),
    /**
     * 댓글 게시 실패
     */
    FAIL_COMMENT_INSERT(false, "FAIL_COMMENT_INSERT", -4004),
    /**
     * 댓글 미입력
     */
    BLANK_COMMENT(false, "BLANK_COMMENT", -4005),
    /**
     * 10 초 이내에 연속 작성
     */
    LIMIT_INTERVAL(false, "เขียนข้อความเพิ่มเติมได้หลัง 10 วินาทีค่ะ กรุณารอสักครู่ค่ะ.", -4006),
    /**
     * 댓글 금지어
     */
    BANNED_WORD(false, "ในข้อความที่ท่านเขียน มีคำห้ามอยู่ค่ะ กรุณาแก้ไขด้วยนะค่ะ.", -4007),
    /**
     * 코멘트의 문자 오버
     */
    TOO_LONG_COMMENT(false, "ท่านสามารถเขียนข้อความได้ไม่เกิน 400 คำ.", -4008),
    /**
     * 타인 기록 삭제
     */
    NO_PERMISSION(false, "REQUIRE_OWNERSHIP", -4009),
    /**
     * 기간 외
     */
    AFTER_PERIODS(false, "AFTER_PERIODS", -4010),
    /**
     * 등록 가능 건수 오버
     */
    FULL(false, "REACH_LIMIT", -4011),
    /**
     * 제한
     */
    RESTRICT(false, "RESTRICT", -4012),
    /**
     * 디바이스 초과
     */
    LIMIT_DEVICE(false, "ท่านสามารถลงทะเบียนอุปกรณ์มือถือได้ถึง 3 เครื่อง กรณีลงแอพใหม่อาจมีการลงอุปกรณ์ซ้ำได้นะค่ะ ท่านสามารถลบอุปกรณ์ที่ไม่ใช้ในเมนูการจัดการอุปกรณ์ค่ะ .", -4013),
    /**
     * 삭제 실패
     */
    FAIL_DELETE(false, "FAIL_DELETE", -4014),
    /**
     * paycoId certification 연동이 실패한 경우
     */
    INVALID_PAYCOID_CERTIFICATION_ERROR(false, " 본인인확인 실패", -1006),
    /**
     * 즐겨 찾기에 등록 실패
     */
    FAIL_FAVORITE(false, "FAIL_FAVORITE", -4015),
    /**
     * 댓글 통보 실패
     */
    FAIL_REPORT_COMMENT(false, "FAIL_REPORT_COMMENT", -4016),
    /**
     * 해당 레코드가없는
     */
    DATA_NOT_FOUND(false, "DATA_NOT_FOUND", -4017),
    /**
     * 로그 등록 실패
     */
    FAIL_LOGGING(false, "FAIL_LOGGING", -4018),
    /**
     * 검증 중, 정지 중 숨겨진 중
     */
    NOT_DISPLAY(false, "NOT_DISPLAY", -4019),
    /**
     * 즐겨찾기 카운트 초과
     */
    FAVORITE_LIMIT(false, "ท่านสามารถลงทะเบียนการูตูนที่อ่านบ่อย (favorate) ได้จำนวน 50 การ์ตูน.", -4020),
    /**
     * 업데이트 실패
     */
    FAIL_UPDATE(false, "FAIL_UPDATE", -4021),
    /**
     * 삽입 실패
     */
    FAIL_INSERT(false, "FAIL_INSERT", -4022),
    /**
     * 컨트롤러에 ComicoUser 파라미터가 없을 때
     */
    CONTROLLER_PARAMETER_MISSING(false, "CONTROLLER_PARAMETER_MISSING", -4023),
    /**
     * 획득하지 않은 뱃지로 업데이트 할 경우
     */
    HAVE_NOT_BADGE(false, "HAVE_NOT_BADGE", -4024),
    /**
     * 데이터가 없을 경우
     */
    NO_DATA(true, "NO_DATA", -4025),
    /**
     * 포인트 충전 플랫폼 값이 유효하지 않는 경우
     */
    INVALID_POINT_CHARGE_PLATFORM(false, "INVALID_POINT_CHARGE_PLATFORM", -4026),
    /**
     * 디바이스 토큰이 유효하지 않은 경우
     */
    INVALID_DEVICE_NAME(false, "INVALID_DEVICE_NAME", -4027),
    /**
     * 디바이스 토큰이 유효하지 않은 경우
     */
    INVALID_DEVICE_IDENTIFIER(false, "INVALID_DEVICE_IDENTIFIER", -4028),


    /**
     * 챕터 좋아요 실패
     */
    FAIL_LIKE_CHAPTER(false, "FAIL_LIKE_CHAPTER", -4029),
    /**
     * 챕터 중복 좋아요
     */
    ALREADY_LIKE_CHAPTER(false, "ALREADY_LIKE_CHAPTER", -4030),
    /**
     * 타이틀 즐겨찾기 중복
     */
    ALREADY_FAVORITE_TITLE(false, "ALREADY_FAVORITE_TITLE", -4031),
    /**
     * 데이터가 없거나 판매 중지된 상품
     */
    NO_DATA_OR_NOT_DISPLAY(false, "NO_DATA_OR_NOT_DISPLAY", -4032),
    /**
     * 자신에게 Block
     */
    BLOCK_SELF(false, "BLOCK_SELF", -4033),
    /**
     * 자동 충전을 이미 하였을 경우
     */
    ALREADY_DAILY_CHARGE(false, "ALREADY_DAILY_CHARGE", -4034),
    /**
     * 패키지 모든 타이틀을 구매한 경우
     */
    ALREADY_PURCHASED_ALL_TITLE(false, "ตอนนี้ท่านได้ซื้อทุกเรื่องในเพคเกจไปแล้ว.", -4035),

    /**
     * 퀘스트 공유하기 제한시간
     */
    LIMIT_TIME_SNS_QUEST(false, "DON'T REGISTER QUEST DURING A HOUR AFTER REGISTERING", -4050),
    FAIL_ENCRYPT(false, "DON'T REGISTER QUEST DURING A HOUR AFTER REGISTERING", -4060),

    FAIL_INSERT_DEVICE(false, "FAIL_INSERT_DEVICE", -4036),

    /**
     * 기타 오류
     */
    UNKNOWN(false, "UNKNOWN", -5000),

    /**
     * 불가능한결제형식
     */
    INVALID_PAYMENT(false, "INVALID_PAYMENT", -6001),
    /**
     * 타이틀 즐겨찾기 중복
     */
    ALREADY_NOTIFY_COMMENT(false, "ท่านได้แจ้งลบข้อความนี้แล้ว", -4061),
    /**
     * 결재잔액부족
     */
    LOW_PAYMENT(false, "LOW_PAYMENT", -6002),
    /**
     * 이미결제한상품
     */
    EXIST_PAYMENT(false, "เป็นการ์ตูนที่ท่านได้ซื้อไปแล้ว.", -6003),
    /**
     * 결제안한상품
     */
    NOT_PURCHASED(false, "NOT_PURCHASED", -6004),
    /**
     * 구매 실패
     */
    FAIL_PURCHASE(false, "เกิด error ในขณะซื้อค่ะ กรุณาลองทำใหม่อีกครั้งนะค่ะ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6005),
    /**
     * 구매 실패
     */
    FAIL_RENT(false, "เกิด error ในขณะซื้อค่ะ กรุณาลองทำใหม่อีกครั้งนะค่ะ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6006),
    /**
     * 결제 완료 처리 실패
     */
    FAIL_BILL_COMPLETE(false, "FAIL_BILL_COMPLETE", -6007),
    /**
     * Toast IAP 결제소비 오류
     */
    FAIL_CONSUME(false, "FAIL_CONSUME", -6010),
    /**
     * Toast IAP 파라미터 오류
     */
    FAIL_PARAMETER_WRONG(false, "FAIL_PARAMETER_WRONG", -6011),
    /**
     * Toast IAP 유효 하지 않는 토큰 정보
     */
    FAIL_INVALID_TOKEN(false, "FAIL_INVALID_TOKEN", -6012),
    /**
     * Toast IAP 유효 하지 않는 결제 번호
     */
    FAIL_INVALID_PAYMENT(false, "FAIL_INVALID_PAYMENT", -6013),
    /**
     * Toast IAP 시스템 애러
     */
    IAP_SYSTEM_ERROR(false, "IAP_SYSTEM_ERROR", -6014),
    /**
     * PAYCO Billing 파라메터 오류
     */
    PAYCO_BILL_INVALID_PARAM(false, "ข้อมูล parameter ไม่ถูกต้องค่ะ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6020),
    /**
     * PAYCO Billing 시스템 애러
     */
    PAYCO_BILL_SYSTEM_ERROR(false, "เกิด error ในระบบการจ่ายค่ะ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6021),
    /**
     * PAYCO Billing 인증 애러
     */
    PAYCO_BILL_AUTH_ERROR(false, "ข้อมูลการยืนยันไม่ถูกต้องค่ะ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6022),
    /**
     * PAYCO Billing 주문 상태 애러
     */
    PAYCO_INVALID_STATE(false, "ไม่มีข้อมูลการซื้อค่ะ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะง.", -6023),
    /**
     * PAYCO Billing 주문번호 애러
     */
    PAYCO_INVALID_ORDER(false, "ไม่พบหมายเขตการซื้อค่ะ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6024),
    /**
     * PAYCO 코미코 주문번호 애러
     */
    PAYCO_INVALID_COMICO_BILL(false, "ไม่พบหมายเขตการซื้อโคมิโก้ค่ะ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6025),
    /**
     * PAYCO 판매자 정보 애러
     */
    PAYCO_INVALID_SELLER(false, "ไม่มีข้อมูลผู้จำหน่ายค่ะ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6026),
    /**
     * PAYCO 주문 예약 실패
     */
    PAYCO_FAIL_RESERVE_ORDER(false, "ไม่สามารถจองการซื้อได้ในขณะนี้ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6027),
    /**
     * PAYCO 주문서 처리 실패
     */
    PAYCO_FAIL_ORDER(false, "ไม่สามารถทำการส่งใบการซื้อได้ในขณะนี้ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6028),
    /**
     * PAYCO 유효하지 않은 금액
     */
    PAYCO_INVALID_PRICE(false, "เป็นจำนวนเงินที่ไม่สามารถจ่ายได้ หากเกิด error ต่อเนื่อง ท่านก็สามารถสอบถาม/ขอความช่วยเหลือ 1:1 ได้นะค่ะ.", -6029),

    /**
     * 이미 사용된 쿠폰
     */
    USED_COUPON(false, "เป็นเลขคูปองที่มีการใช้แล้ว กรุณาตรวจสอบเลขคูปองใหม่อีกครั้งค่ะ.", -7001),
    /**
     * 유효하지 않은 쿠폰
     */
    INVALID_COUPON(false, "เป็นเลขคูปองที่ใช้ไม่ได้แล้ว กรุณาตรวจสอบเลขคูปองใหม่อีกครั้งค่ะ.", -7002),
    /**
     * 사용기간이 지난 쿠폰
     */
    EXPIRED_COUPON(false, "เป็นเลขคูปองที่หมดเวลาการใช้แล้ว กรุณาตรวจสอบเลขคูปองใหม่อีกครั้งค่ะ.", -7003),
    /**
     * 이미 사용한 계정
     */
    USED_USER_COUPON(false, "ท่านเกินจำนวนที่ใช้คูปองประเภทเดียวกันได้ กรุณาตรวจสอบเลขคูปองใหม่อีกครั้งค่ะ.", -7004),
    /**
     * 유효하지 않은 쿠폰 아이템
     */
    INVALID_COUPON_ITEM(false, "INVALID_COUPON_ITEM", -7005),

    /**
     * 프로모션 확인 요청 실패
     */
    FAIL_PROMOTION_REQUEST(false, "FAIL_PROMOTION_REQUEST", -8001),
    /**
     * 프로모션 보상 내역이 없음
     */
    EMPTY_PROMOTION_REWARD(false, "EMPTY_PROMOTION_REWARD", -8002),
    /**
     * 프로모션 미션 키가 미등록
     */
    INVALID_REWARD_CODE(false, "INVALID_REWARD_CODE", -8003),

    /**
     * 유효하지 않는 제품
     */
    INVALID_PRODUCT(false, "INVALID_PRODUCT", -9001),
    INVALID_BILL_PARAMETER(false, "INVALID_BILL_PARAMETER", -9002),
    INVALID_DEVICE_TYPE(false, "INVALID_DEVICE_TYPE", -9003),

    /**
     * 등록되지 않은 서버 IP
     */
    NOT_REGISTED_WEBSERVER_IP(false, "NOT_REGISTED_WEBSERVER_IP", -9004),
    /**
     * 삭제실패
     */
    FAIL_DELETE_WEBSERVER_IP(false, "FAIL_DELETE_WEBSERVER_IP", -9005),
    /**
     * 등록되지 않는 결제 서버 아이피
     */
    NOT_REGISTED_BILLSERVER_IP(false, "NOT_REGISTED_BILLSERVER_IP", -9006),
    /**
     * Mol reason code
     */

    MOL_SUCCESS(true, "Success", 0),
    MOL_INVALID_PIN(false, "Invalid PIN", 100),
    MOL_CARD_OR_SEIRAL_WAS_USED(false, "Card or Serial was used", 101),
    MOL_INVALID_DUPLICATED_MERCHANT_REFERENCE_ID(false, "Invalid/Duplicated Merchant Reference ID", 102),
    MOL_INVALID_SIGNATURE(false, "Invalid Signature", 103),
    MOL_INVALID_MERCHANT_ID(false, "Invalid Merchant ID", 104),
    MOL_INVALID_INPUT(false, "Invalid Input", 105),
    MOL_INVALID_GAME_ID(false, "Invalid Game ID", 106),
    MOL_SERIAL_NOT_FOUND(false, "Serial not found", 107),
    MOL_PAYMENT_CHANEL_NOT_FOUND(false, "Payment channel not found", 108),
    MOL_TRANSACTION_NOT_FOUND(false, "Transaction not found", 109),
    MOL_CARD_OR_PIN_WAS_EXPIRED(false, "Card or PIN was expired", 200),
    MOL_CARD_OR_PIN_IS_NOT_AVALABLE(false, "Card or PIN is not available", 201),
    MOL_INVALID_PROVIDER_ID(false, "Invalid provider ID", 801),
    MOL_INVALID_CHANNEL_ID(false, "Invalid channel ID", 802),
    MOL_SYSTEM_ERROR(false, "System error", 803),
    MOL_BACKEND_RETURN_FAIL(false, "Back-end return fail", 804),
    MOL_BACKEND_RETURN_ERROR(false, "Back-end return error", 805),
    MOL_SYSTEM_UNDER_MAINTENANCE(false, "System under maintenance", 900),
    MOL_OPERATION_TIMED_OUT(false, "Operation timed out", 901),
    MOL_INTERNAL_ERROR(false, "Internal error", 902),

    INVALID_EMAIL_TOKEN(false, "Invalid email verification token", -10001),
    EXPIRED_EMAIL_TOKEN(false, "Expired email verification token", -10002),
    VERIFIED_EMAIL_TOKEN(false, "Verified email verification token", -10003),
    ALREADY_VERIFIED_EMAIL(false, "Already verified email", -10004),
    ALREADY_IN_USE_EMAIL(false, "Already in use email", -10005);


    private final boolean isSuccessful;
    private String resultMessage;
    private int resultCode;

    ResponseType(boolean isSuccessful, String resultMessage, int resultCode) {
        this.isSuccessful = isSuccessful;
        this.resultMessage = resultMessage;
        this.resultCode = resultCode;
    }

    public boolean getIsSuccessful() {
        return isSuccessful;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResutlCode(int resultCode) {
        this.resultCode = resultCode;
    }
}