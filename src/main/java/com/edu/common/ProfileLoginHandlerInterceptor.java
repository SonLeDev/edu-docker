package com.edu.common;

import com.edu.domain.ProfileSignIn;
import com.edu.repository.ProfileSignInRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by sstvn on 5/2/17.
 */
public class ProfileLoginHandlerInterceptor implements HandlerInterceptor {

    @Autowired
    ProfileSignInRepository profileSignInRepository;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        //TODO : verify userId - tokenId

        String accessToken = httpServletRequest.getHeader("accessToken");
        ProfileSignIn signIn = profileSignInRepository.findByAccessToken(accessToken);
        if(signIn!=null){
            return true;
        }
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
