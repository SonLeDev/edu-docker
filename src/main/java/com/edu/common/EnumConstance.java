package com.edu.common;

/**
 * Created by sstvn on 5/1/17.
 */
public class EnumConstance {
    public enum EDU_CLASSES{
        CLASS_10("10"),CLASS_11("11"),CLASS_12("12"),CLASS_13("13");
        private String name;
        EDU_CLASSES(String name){
            this.name = name;
        }
        String getName(){return name;}
    }

    public enum QUIZ_TYPE{
        EXAM_QUIZ("EduExamQuiz"),PRAC_QUIZ("EduPracticeQuiz");

        private String name;
        QUIZ_TYPE(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

}
