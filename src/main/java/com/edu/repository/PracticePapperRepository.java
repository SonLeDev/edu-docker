package com.edu.repository;

import com.edu.domain.PracticePapper;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * Created by sstvn on 5/2/17.
 */
public interface PracticePapperRepository extends JpaRepository<PracticePapper,Long> {
    //Collection<PracticePapper> findByClassIdAndSubjectId();
}
