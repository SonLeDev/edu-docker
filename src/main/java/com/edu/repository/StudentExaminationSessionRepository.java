package com.edu.repository;

import com.edu.domain.StudentExaminationSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by SonLee on 5/3/2017.
 */
public interface StudentExaminationSessionRepository extends JpaRepository<StudentExaminationSession,Long> {

}
