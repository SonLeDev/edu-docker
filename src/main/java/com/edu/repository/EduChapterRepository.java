package com.edu.repository;

import com.edu.domain.EduChapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by sstvn on 5/22/17.
 */
public interface EduChapterRepository extends JpaRepository<EduChapter,Long> {
    @Query(value = "SELECT * FROM edu_chapter WHERE subject_id = ?1 AND chapter_no = ?2",nativeQuery = true)
    List<EduChapter> findBySubjectIdAndChapterNo(Integer subjectId,Integer chapterNo);
}
