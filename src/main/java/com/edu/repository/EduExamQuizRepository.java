package com.edu.repository;

import com.edu.domain.EduExamQuiz;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sstvn on 5/7/17.
 */
public interface EduExamQuizRepository extends JpaRepository<EduExamQuiz,Long> {
}
