package com.edu.repository;

import com.edu.domain.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sstvn on 5/1/17.
 */
public interface ProfileRepository extends JpaRepository<Profile,Long> {
    Profile findByFacebookId(String fbTocken);

    Profile findByGoogleId(String ggToken);
}
