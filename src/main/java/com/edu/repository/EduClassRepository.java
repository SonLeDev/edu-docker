package com.edu.repository;

import com.edu.domain.EduClass;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by sstvn on 5/2/17.
 */
public interface EduClassRepository extends CrudRepository<EduClass,Long> {
}