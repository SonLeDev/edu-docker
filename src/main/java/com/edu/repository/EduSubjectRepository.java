package com.edu.repository;

import com.edu.domain.EduSubject;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by sstvn on 5/2/17.
 */
public interface EduSubjectRepository extends CrudRepository<EduSubject,Long> {

    List<EduSubject> findByClassName(String className);

}
