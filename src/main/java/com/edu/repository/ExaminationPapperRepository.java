package com.edu.repository;

import com.edu.domain.ExaminationPapper;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * Created by sstvn on 5/2/17.
 */
public interface ExaminationPapperRepository extends JpaRepository<ExaminationPapper,Long> {

    Collection<ExaminationPapper> findByClassIdAndSubjectId(Long classId, Long subjectId);
}
