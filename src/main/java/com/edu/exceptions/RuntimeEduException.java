package com.edu.exceptions;

import com.edu.common.ResponseType;
import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Component
public class RuntimeEduException extends RuntimeException {
    public static final RuntimeEduException SUCCESS = new RuntimeEduException(ResponseType.SUCCESS);
    public static final RuntimeEduException REQUIRED_TOKEN = new RuntimeEduException(
            ResponseType.REQUIRED_TOKEN);
    public static final RuntimeEduException EMPTY_SNS_TOKEN = new RuntimeEduException(
            ResponseType.EMPTY_SNS_TOKEN);
    public static final RuntimeEduException PURCHASE_INVALID_METHOD = new RuntimeEduException(ResponseType.PURCHASE_INVALID_METHOD);
    public static final RuntimeEduException INVALID_PAYCOID_CERTIFICATION_ERROR = new RuntimeEduException(
            ResponseType.INVALID_PAYCOID_CERTIFICATION_ERROR);
    public static final RuntimeEduException NOT_REGISTER = new RuntimeEduException(ResponseType.NOT_REGISTER);
    public static final RuntimeEduException ALREADY_REGISTER = new RuntimeEduException(ResponseType.ALREADY_REGISTER);
    public static final RuntimeEduException INVALID_NICKNAME = new RuntimeEduException(ResponseType.INVALID_NICKNAME);
    public static final RuntimeEduException INVALID_DUPLICATE_NICKNAME = new RuntimeEduException(ResponseType.INVALID_DUPLICATE_NICKNAME);
    public static final RuntimeEduException INVALID_INVALIDWORD_NICKNAME = new RuntimeEduException(ResponseType.INVALID_INVALIDWORD_NICKNAME);
    public static final RuntimeEduException INVALID_PARAMETER = new RuntimeEduException(ResponseType.INVALID_PARAMETER);
    public static final RuntimeEduException INVALID_TOKEN = new RuntimeEduException(
            ResponseType.INVALID_TOKEN);
    public static final RuntimeEduException INVALID_OPENID_TOKEN = new RuntimeEduException(
            ResponseType.INVALID_OPENID_TOKEN);
    public static final RuntimeEduException INVALID_HEADER = new RuntimeEduException(
            ResponseType.INVALID_HEADER);
    public static final RuntimeEduException ALREADY_LIKE_COMMENT = new RuntimeEduException(ResponseType.ALREADY_LIKE_COMMENT);
    public static final RuntimeEduException ALREADY_LIKE_CHAPTER = new RuntimeEduException(
            ResponseType.ALREADY_LIKE_CHAPTER);
    public static final RuntimeEduException ALREADY_FAVORITE_TITLE = new RuntimeEduException(
            ResponseType.ALREADY_FAVORITE_TITLE);
    public static final RuntimeEduException FAIL_LIKE_COMMENT = new RuntimeEduException(
            ResponseType.FAIL_LIKE_COMMENT);
    public static final RuntimeEduException FAIL_LIKE_CHAPTER = new RuntimeEduException(
            ResponseType.FAIL_LIKE_CHAPTER);
    public static final RuntimeEduException LIKE_SELF = new RuntimeEduException(ResponseType.LIKE_SELF);
    public static final RuntimeEduException FAIL_COMMENT_INSERT = new RuntimeEduException(ResponseType.FAIL_COMMENT_INSERT);
    public static final RuntimeEduException BLANK_COMMENT = new RuntimeEduException(ResponseType.BLANK_COMMENT);
    public static final RuntimeEduException LIMIT_INTERVAL = new RuntimeEduException(ResponseType.LIMIT_INTERVAL);
    public static final RuntimeEduException BANNED_WORD = new RuntimeEduException(ResponseType.BANNED_WORD);
    public static final RuntimeEduException TOO_LONG_COMMENT = new RuntimeEduException(ResponseType.TOO_LONG_COMMENT);
    public static final RuntimeEduException NO_PERMISSION = new RuntimeEduException(ResponseType.NO_PERMISSION);
    public static final RuntimeEduException AFTER_PERIODS = new RuntimeEduException(ResponseType.AFTER_PERIODS);
    public static final RuntimeEduException FULL = new RuntimeEduException(ResponseType.FULL);
    public static final RuntimeEduException RESTRICT = new RuntimeEduException(ResponseType.RESTRICT);
    public static final RuntimeEduException LIMIT_DEVICE = new RuntimeEduException(ResponseType.LIMIT_DEVICE);
    public static final RuntimeEduException FAIL_DELETE = new RuntimeEduException(ResponseType.FAIL_DELETE);
    public static final RuntimeEduException FAIL_FAVORITE = new RuntimeEduException(ResponseType.FAIL_FAVORITE);
    public static final RuntimeEduException FAIL_REPORT_COMMENT = new RuntimeEduException(ResponseType.FAIL_REPORT_COMMENT);
    public static final RuntimeEduException DATA_NOT_FOUND = new RuntimeEduException(ResponseType.DATA_NOT_FOUND);
    public static final RuntimeEduException FAIL_LOGGING = new RuntimeEduException(ResponseType.FAIL_LOGGING);
    public static final RuntimeEduException NOT_DISPLAY = new RuntimeEduException(ResponseType.NOT_DISPLAY);
    public static final RuntimeEduException FAVORITE_LIMIT = new RuntimeEduException(ResponseType.FAVORITE_LIMIT);
    public static final RuntimeEduException FAIL_UPDATE = new RuntimeEduException(ResponseType.FAIL_UPDATE);
    public static final RuntimeEduException FAIL_INSERT = new RuntimeEduException(ResponseType.FAIL_INSERT);
    public static final RuntimeEduException UNKNOWN = new RuntimeEduException(ResponseType.UNKNOWN);
    public static final RuntimeEduException INVALID_PAYMENT = new RuntimeEduException(ResponseType.INVALID_PAYMENT);
    public static final RuntimeEduException LOW_PAYMENT = new RuntimeEduException(ResponseType.LOW_PAYMENT);
    public static final RuntimeEduException EXIST_PAYMENT = new RuntimeEduException(ResponseType.EXIST_PAYMENT);
    public static final RuntimeEduException HAVE_NOT_BADGE = new RuntimeEduException(ResponseType.HAVE_NOT_BADGE);
    public static final RuntimeEduException CONTROLLER_PARAMETER_MISSING = new RuntimeEduException(ResponseType.CONTROLLER_PARAMETER_MISSING);
    public static final RuntimeEduException NOT_PURCHASED = new RuntimeEduException(ResponseType.NOT_PURCHASED);
    public static final RuntimeEduException LIMIT_TIME_SNS_QUEST = new RuntimeEduException(ResponseType.LIMIT_TIME_SNS_QUEST);
    public static final RuntimeEduException NO_DATA = new RuntimeEduException(ResponseType.NO_DATA);
    public static final RuntimeEduException FAIL_PURCHASE = new RuntimeEduException(ResponseType.FAIL_PURCHASE);
    public static final RuntimeEduException FAIL_RENT = new RuntimeEduException(ResponseType.FAIL_RENT);
    public static final RuntimeEduException INVALID_POINT_CHARGE_PLATFORM = new RuntimeEduException(
            ResponseType.INVALID_POINT_CHARGE_PLATFORM);
    public static final RuntimeEduException INVALID_DEVICE_IDENTIFIER = new RuntimeEduException(
            ResponseType.INVALID_DEVICE_IDENTIFIER);
    public static final RuntimeEduException FAIL_INSERT_DEVICE = new RuntimeEduException(
            ResponseType.FAIL_INSERT_DEVICE);
    public static final RuntimeEduException INVALID_DEVICE_NAME = new RuntimeEduException(
            ResponseType.INVALID_DEVICE_NAME);
    public static final RuntimeEduException NO_DATA_OR_NOT_DISPLAY = new RuntimeEduException(ResponseType.NO_DATA_OR_NOT_DISPLAY);
    public static final RuntimeEduException FAIL_BILL_COMPLETE = new RuntimeEduException(ResponseType.FAIL_BILL_COMPLETE);
    public static final RuntimeEduException FAIL_CONSUME = new RuntimeEduException(ResponseType.FAIL_CONSUME);
    public static final RuntimeEduException FAIL_PARAMETER_WRONG = new RuntimeEduException(ResponseType.FAIL_PARAMETER_WRONG);
    public static final RuntimeEduException FAIL_INVALID_TOKEN = new RuntimeEduException(ResponseType.FAIL_INVALID_TOKEN);
    public static final RuntimeEduException FAIL_INVALID_PAYMENT = new RuntimeEduException(ResponseType.FAIL_INVALID_PAYMENT);
    public static final RuntimeEduException IAP_SYSTEM_ERROR = new RuntimeEduException(ResponseType.IAP_SYSTEM_ERROR);
    public static final RuntimeEduException PAYCO_BILL_INVALID_PARAM = new RuntimeEduException(ResponseType.PAYCO_BILL_INVALID_PARAM);
    public static final RuntimeEduException PAYCO_BILL_SYSTEM_ERROR = new RuntimeEduException(ResponseType.PAYCO_BILL_SYSTEM_ERROR);
    public static final RuntimeEduException PAYCO_BILL_AUTH_ERROR = new RuntimeEduException(ResponseType.PAYCO_BILL_AUTH_ERROR);
    public static final RuntimeEduException PAYCO_INVALID_STATE = new RuntimeEduException(ResponseType.PAYCO_INVALID_STATE);
    public static final RuntimeEduException PAYCO_INVALID_ORDER = new RuntimeEduException(ResponseType.PAYCO_INVALID_ORDER);
    public static final RuntimeEduException PAYCO_INVALID_COMICO_BILL = new RuntimeEduException(ResponseType.PAYCO_INVALID_COMICO_BILL);
    public static final RuntimeEduException PAYCO_INVALID_SELLER = new RuntimeEduException(ResponseType.PAYCO_INVALID_SELLER);
    public static final RuntimeEduException PAYCO_FAIL_RESERVE_ORDER = new RuntimeEduException(ResponseType.PAYCO_FAIL_RESERVE_ORDER);
    public static final RuntimeEduException PAYCO_FAIL_ORDER = new RuntimeEduException(ResponseType.PAYCO_FAIL_ORDER);
    public static final RuntimeEduException PAYCO_INVALID_PRICE = new RuntimeEduException(ResponseType.PAYCO_INVALID_PRICE);
    public static final RuntimeEduException MOL_SUCCESS = new RuntimeEduException(ResponseType.MOL_SUCCESS);
    public static final RuntimeEduException MOL_INVALID_PIN = new RuntimeEduException(ResponseType.MOL_INVALID_PIN);
    public static final RuntimeEduException MOL_CARD_OR_SEIRAL_WAS_USED = new RuntimeEduException(ResponseType.MOL_CARD_OR_SEIRAL_WAS_USED);
    public static final RuntimeEduException MOL_INVALID_DUPLICATED_MERCHANT_REFERENCE_ID = new RuntimeEduException(ResponseType.MOL_INVALID_DUPLICATED_MERCHANT_REFERENCE_ID);
    public static final RuntimeEduException MOL_INVALID_SIGNATURE = new RuntimeEduException(ResponseType.MOL_INVALID_SIGNATURE);
    public static final RuntimeEduException MOL_INVALID_MERCHANT_ID = new RuntimeEduException(ResponseType.MOL_INVALID_MERCHANT_ID);
    public static final RuntimeEduException MOL_INVALID_INPUT = new RuntimeEduException(ResponseType.MOL_INVALID_INPUT);
    public static final RuntimeEduException MOL_INVALID_GAME_ID = new RuntimeEduException(ResponseType.MOL_INVALID_GAME_ID);
    public static final RuntimeEduException MOL_SERIAL_NOT_FOUND = new RuntimeEduException(ResponseType.MOL_SERIAL_NOT_FOUND);
    public static final RuntimeEduException MOL_PAYMENT_CHANEL_NOT_FOUND = new RuntimeEduException(ResponseType.MOL_PAYMENT_CHANEL_NOT_FOUND);
    public static final RuntimeEduException MOL_TRANSACTION_NOT_FOUND = new RuntimeEduException(ResponseType.MOL_TRANSACTION_NOT_FOUND);
    public static final RuntimeEduException MOL_CARD_OR_PIN_WAS_EXPIRED = new RuntimeEduException(ResponseType.MOL_CARD_OR_PIN_WAS_EXPIRED);
    public static final RuntimeEduException MOL_CARD_OR_PIN_IS_NOT_AVALABLE = new RuntimeEduException(ResponseType.MOL_CARD_OR_PIN_IS_NOT_AVALABLE);
    public static final RuntimeEduException MOL_INVALID_PROVIDER_ID = new RuntimeEduException(ResponseType.MOL_INVALID_PROVIDER_ID);
    public static final RuntimeEduException MOL_INVALID_CHANNEL_ID = new RuntimeEduException(ResponseType.MOL_INVALID_CHANNEL_ID);
    public static final RuntimeEduException MOL_SYSTEM_ERROR = new RuntimeEduException(ResponseType.MOL_SYSTEM_ERROR);
    public static final RuntimeEduException MOL_BACKEND_RETURN_FAIL = new RuntimeEduException(ResponseType.MOL_BACKEND_RETURN_FAIL);
    public static final RuntimeEduException MOL_BACKEND_RETURN_ERROR = new RuntimeEduException(ResponseType.MOL_BACKEND_RETURN_ERROR);
    public static final RuntimeEduException MOL_SYSTEM_UNDER_MAINTENANCE = new RuntimeEduException(ResponseType.MOL_SYSTEM_UNDER_MAINTENANCE);
    public static final RuntimeEduException MOL_OPERATION_TIMED_OUT = new RuntimeEduException(ResponseType.MOL_OPERATION_TIMED_OUT);
    public static final RuntimeEduException MOL_INTERNAL_ERROR = new RuntimeEduException(ResponseType.MOL_INTERNAL_ERROR);


    public static final RuntimeEduException BLOCK_SELF = new RuntimeEduException(
            ResponseType.BLOCK_SELF);
    public static final RuntimeEduException FAIL_PROMOTION_REQUEST = new RuntimeEduException(ResponseType.FAIL_PROMOTION_REQUEST);
    public static final RuntimeEduException EMPTY_PROMOTION_REWARD = new RuntimeEduException(
            ResponseType.EMPTY_PROMOTION_REWARD);
    public static final RuntimeEduException INVALID_REWARD_CODE = new RuntimeEduException(
            ResponseType.INVALID_REWARD_CODE);
    public static final RuntimeEduException ALREADY_DAILY_CHARGE = new RuntimeEduException(
            ResponseType.ALREADY_DAILY_CHARGE);
    public static final RuntimeEduException FAIL_ENCRYPT = new RuntimeEduException(ResponseType.FAIL_ENCRYPT);
    public static final RuntimeEduException ALREADY_PURCHASED_ALL_TITLE = new RuntimeEduException(ResponseType.ALREADY_PURCHASED_ALL_TITLE);
    public static final RuntimeEduException NOT_REGISTED_WEBSERVER_IP = new RuntimeEduException(
            ResponseType.NOT_REGISTED_WEBSERVER_IP);
    public static final RuntimeEduException FAIL_DELETE_WEBSERVER_IP = new RuntimeEduException(
            ResponseType.FAIL_DELETE_WEBSERVER_IP);
    public static final RuntimeEduException NOT_REGISTED_BILLSERVER_IP = new RuntimeEduException(
            ResponseType.NOT_REGISTED_BILLSERVER_IP);

    public static final RuntimeEduException INVALID_EMAIL_TOKEN = new RuntimeEduException(ResponseType.INVALID_EMAIL_TOKEN);
    public static final RuntimeEduException EXPIRED_EMAIL_TOKEN = new RuntimeEduException(ResponseType.EXPIRED_EMAIL_TOKEN);
    public static final RuntimeEduException VERIFIED_EMAIL_TOKEN = new RuntimeEduException(ResponseType.VERIFIED_EMAIL_TOKEN);
    public static final RuntimeEduException ALREADY_VERIFIED_EMAIL = new RuntimeEduException(ResponseType.ALREADY_VERIFIED_EMAIL);
    public static final RuntimeEduException ALREADY_IN_USE_EMAIL = new RuntimeEduException(ResponseType.ALREADY_IN_USE_EMAIL);

    public static final RuntimeEduException NEOID_LOGIN_ERROR = new RuntimeEduException(ResponseType.NEOID_LOGIN_ERROR);

    protected ResponseType responseType = ResponseType.UNKNOWN;

    public RuntimeEduException() {

    }

    public RuntimeEduException(ResponseType responseType) {
        super(responseType.getResultMessage());
        this.responseType = responseType;
    }

    public static final RuntimeEduException ALREADY_NOTIFY_COMMENT = new RuntimeEduException(
            ResponseType.ALREADY_NOTIFY_COMMENT);

    public RuntimeEduException(ResponseType responseType, Throwable e) {
        super(responseType.getResultMessage(), e);
        this.responseType = responseType;
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public int getResultCode() {
        return responseType.getResultCode();
    }

    public boolean getIsSuccessful() {
        return responseType.getIsSuccessful();
    }

    public String getResultMessage() {
        return responseType.getResultMessage();
    }
}