package com.edu.pattern;


import com.edu.domain.Profile;
import com.edu.domain.ProfileSignIn;
import com.edu.domain.StudentClass;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by sstvn on 5/1/17.
 */
public class ProfileBuilder {

    private Profile profile;
    private StudentClass studentClass;
    private ProfileSignIn proSignIn;

    private ProfileBuilder(){
        profile = new Profile();
    }


    public static ProfileBuilder newInstance(){
        return new ProfileBuilder();
    }

    public ProfileBuilder setUserName(String userName){
        profile.setUserName(userName);
        return this;
    }

    public ProfileBuilder setFbToken(String fbToken){
        profile.setFacebookId(fbToken);
        return this;
    }

    public ProfileBuilder setGGToken(String ggToken){
        profile.setGoogleId(ggToken);
        return this;
    }

    public Profile build(){
        return profile;
    }


    public Profile register() {
        if(StringUtils.isEmpty(profile.getUserName())){
            profile.setUserName("Student");
        }
        return profile;
    }

    public static ProfileBuilder initInstance(Profile profile) {
        ProfileBuilder profileBuilder = new ProfileBuilder();
        profileBuilder.profile = profile;
        return profileBuilder;
    }

    public StudentClass becomeStudent(String className) {
        studentClass = new StudentClass();
        studentClass.setProfileId(this.profile.getUserId());
        studentClass.setJoinClass(className);
        return studentClass;
    }

    public ProfileSignIn signIn(String accessToken,String deviceId) {
        proSignIn = new ProfileSignIn();
        proSignIn.setProfileId(this.profile.getUserId());
        proSignIn.setAccessToken(accessToken);
        proSignIn.setDeviceId(deviceId);
        return  proSignIn;
    }

    public void updateProfile(Profile profile) {
        this.profile = profile;
    }
}
