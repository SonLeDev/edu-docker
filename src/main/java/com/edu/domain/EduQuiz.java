package com.edu.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by sstvn on 5/2/17.
 */
@Entity
public class EduQuiz {
    @Id
    @GeneratedValue
    private Long id;

    private int noOfQuest;

    private String question;

    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;

    private String correctAnswer;
    private int level;
    private String solution;
    private String detailAnswer;
    private String explainAnswer;
    private Float pointOfQuiz;

    private Long imageId;


    private Long chapterId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswerA() {
        return answerA;
    }

    public void setAnswerA(String answerA) {
        this.answerA = answerA;
    }

    public String getAnswerB() {
        return answerB;
    }

    public void setAnswerB(String answerB) {
        this.answerB = answerB;
    }

    public String getAnswerC() {
        return answerC;
    }

    public void setAnswerC(String answerC) {
        this.answerC = answerC;
    }

    public String getAnswerD() {
        return answerD;
    }

    public void setAnswerD(String answerD) {
        this.answerD = answerD;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getDetailAnswer() {
        return detailAnswer;
    }

    public void setDetailAnswer(String detailAnswer) {
        this.detailAnswer = detailAnswer;
    }

    public String getExplainAnswer() {
        return explainAnswer;
    }

    public void setExplainAnswer(String explainAnswer) {
        this.explainAnswer = explainAnswer;
    }

    public int getNoOfQuest() {
        return noOfQuest;
    }

    public void setNoOfQuest(int noOfQuest) {
        this.noOfQuest = noOfQuest;
    }

    public Float getPointOfQuiz() {
        return pointOfQuiz;
    }

    public void setPointOfQuiz(Float pointOfQuiz) {
        this.pointOfQuiz = pointOfQuiz;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public Long getChapterId() {
        return chapterId;
    }

    public void setChapterId(Long chapterId) {
        this.chapterId = chapterId;
    }
}
