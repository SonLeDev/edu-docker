package com.edu.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by sstvn on 5/1/17.
 */
@Entity
public class StudentPracticeSession {

    @Id
    @GeneratedValue
    private Long id;
    private Date dateTakePrac;
    private Long subjectId;     // option
    private Long chapaterId;    // option

    private Long profileId;     // studenId
    private Long praticeId;     // take pracId
    private Integer point;
    private Boolean complete;   // status

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateTakePrac() {
        return dateTakePrac;
    }

    public void setDateTakePrac(Date dateTakePrac) {
        this.dateTakePrac = dateTakePrac;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getChapaterId() {
        return chapaterId;
    }

    public void setChapaterId(Long chapaterId) {
        this.chapaterId = chapaterId;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public Long getPraticeId() {
        return praticeId;
    }

    public void setPraticeId(Long praticeId) {
        this.praticeId = praticeId;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Boolean getComplete() {
        return complete;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }
}
