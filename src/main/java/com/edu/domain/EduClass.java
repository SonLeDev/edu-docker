package com.edu.domain;

import javax.persistence.*;

/**
 * Created by sstvn on 5/1/17.
 */
@Entity
public class EduClass {
    @Id
    @GeneratedValue
    private Long id;

    private String className;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }


}
