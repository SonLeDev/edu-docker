package com.edu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import java.util.Collection;
import java.util.List;

/**
 * Created by sstvn on 5/1/17.
 */
@Entity
public class EduSubject {
    @Id
    @GeneratedValue
    private Long id;

    private String subjectName;

    /* chapterStr is json format {"id":1,"name":"Real Numbers"}*/
    private String chapterStr;

    private String className;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "eduSubject")
    private List<EduChapter> eduChapters;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getChapterStr() {
        return chapterStr;
    }

    public void setChapterStr(String chapterStr) {
        this.chapterStr = chapterStr;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<EduChapter> getEduChapters() {
        return eduChapters;
    }

    public void setEduChapters(List<EduChapter> eduChapters) {
        this.eduChapters = eduChapters;
    }
}
