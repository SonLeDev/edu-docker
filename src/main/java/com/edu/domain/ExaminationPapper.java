package com.edu.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by sstvn on 5/1/17.
 */
@Entity
public class ExaminationPapper {

    @Id
    @GeneratedValue
    private Long id;
    private String examName;    // option
    private String codeExam;
    private String author;      // who compose, ignore manage teacher
    private int yearExam;
    private int timeExpire;
    private int numOfQuiz;
    private String description;
    private Long classId;     // exam for who belong to classId
    private Long subjectId;     // exam for specific subjectId
    private Integer level;      // the difficulty of the exam
    private String anwserStr;   // exam : 1A2B3CD4D5B6AB....


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "examination_quizs",
            joinColumns = { @JoinColumn(name = "exam_id") },
            inverseJoinColumns = { @JoinColumn(name = "quiz_id") })
    @OrderBy("noOfQuest")
    private Set<EduQuiz> eduQuix ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }


    public int getNumOfQuiz() {
        return numOfQuiz;
    }

    public void setNumOfQuiz(int numOfQuiz) {
        this.numOfQuiz = numOfQuiz;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCodeExam() {
        return codeExam;
    }

    public void setCodeExam(String codeExam) {
        this.codeExam = codeExam;
    }

    public int getYearExam() {
        return yearExam;
    }

    public void setYearExam(int yearExam) {
        this.yearExam = yearExam;
    }

    public int getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(int timeExpire) {
        this.timeExpire = timeExpire;
    }

    public String getAnwserStr() {
        return anwserStr;
    }

    public void setAnwserStr(String anwserStr) {
        this.anwserStr = anwserStr;
    }

    public Set<EduQuiz> getEduQuix() {
        return eduQuix;
    }

    public void setEduQuix(Set<EduQuiz> eduQuix) {
        this.eduQuix = eduQuix;
    }
}
