package com.edu.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by sstvn on 5/1/17.
 */
@Entity
public class StudentClass {

    @Id
    @GeneratedValue
    private Long id;

    private Long profileId;
    private String joinClass;
    private Date dateOfJoin = new Date();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public String getJoinClass() {
        return joinClass;
    }

    public void setJoinClass(String joinClass) {
        this.joinClass = joinClass;
    }

    public Date getDateOfJoin() {
        return dateOfJoin;
    }

    public void setDateOfJoin(Date dateOfJoin) {
        this.dateOfJoin = dateOfJoin;
    }
}
