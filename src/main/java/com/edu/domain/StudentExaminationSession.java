package com.edu.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.Map;

/**
 * Created by sstvn on 5/1/17.
 */

@Entity
public class StudentExaminationSession {
    @Id
    @GeneratedValue
    private Long id;
    private Date dateTakeExam;
    private Long profileId;     // studentId
    private Long examId;        // take examId
    private Integer point;
    private Boolean status;   // status, option
    private String studentAnswer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateTakeExam() {
        return dateTakeExam;
    }

    public void setDateTakeExam(Date dateTakeExam) {
        this.dateTakeExam = dateTakeExam;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getStudentAnswer() {
        return studentAnswer;
    }

    public void setStudentAnswer(String studentAnswer) {
        this.studentAnswer = studentAnswer;
    }
}
