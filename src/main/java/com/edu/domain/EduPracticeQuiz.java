package com.edu.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Created by sstvn on 5/1/17.
 */
@Deprecated
@Entity
public class EduPracticeQuiz {

    @Id
    @GeneratedValue
    private Long id;

    private Long contentId;

    @Transient
    private String[] answer;

    @Transient
    private String[] correctAnswer;


    // difficult
    private int level;

    private String anwserStr;

    private String correctAnswerStr;

    // examination or practice types
    private String quizType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

    public String[] getAnswer() {
        return answer;
    }

    public void setAnswer(String[] answer) {
        this.answer = answer;
    }

    public String[] getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String[] correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getAnwserStr() {
        return anwserStr;
    }

    public void setAnwserStr(String anwserStr) {
        this.anwserStr = anwserStr;
    }

    public String getCorrectAnswerStr() {
        return correctAnswerStr;
    }

    public void setCorrectAnswerStr(String correctAnswerStr) {
        this.correctAnswerStr = correctAnswerStr;
    }

    public String getQuizType() {
        return quizType;
    }

    public void setQuizType(String quizType) {
        this.quizType = quizType;
    }
}
