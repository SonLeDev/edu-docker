package com.edu.domain;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by sstvn on 5/1/17.
 */

@Entity
public class PracticePapper {

    @Id
    @GeneratedValue
    private Long id;

    private Long classId;
    private Long subjectId;
    private Long chapterId;

    private String author; // who compose this practice for this chapterId

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "practice_quizs",
            joinColumns = { @JoinColumn(name = "prac_Id") },
            inverseJoinColumns = { @JoinColumn(name = "quiz_Id") })
    private Set<EduPracticeQuiz> eduPracticeQuizs = new HashSet<>(0);

    public Set<EduPracticeQuiz> getEduPracticeQuizs() {
        return eduPracticeQuizs;
    }

    public void setEduPracticeQuizs(Set<EduPracticeQuiz> quizLst) {
        this.eduPracticeQuizs = quizLst;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChapterId() {
        return chapterId;
    }

    public void setChapterId(Long chapterId) {
        this.chapterId = chapterId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

}
