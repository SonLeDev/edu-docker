package com.edu.service;

import com.edu.domain.StudentExaminationSession;
import com.edu.form.StudentQuizDoneForm;
import com.edu.repository.StudentExaminationSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by SonLee on 5/3/2017.
 */
@Service
public class StudentService {

    @Autowired
    StudentExaminationSessionRepository examinationSessionRepository;

    public StudentExaminationSession takeExamination(Long examId,Long profileId) {
        StudentExaminationSession studentExam = new StudentExaminationSession();
        studentExam.setExamId(examId);
        studentExam.setProfileId(profileId);

        return examinationSessionRepository.save(studentExam);
    }

    public void examDone(StudentQuizDoneForm studentQuizDoneForm) {
        StudentExaminationSession examinationSession = examinationSessionRepository.findOne(studentQuizDoneForm.getSessionId());
        examinationSession.setStatus(true);
        examinationSessionRepository.saveAndFlush(examinationSession);
    }

    public void analyseResult(StudentQuizDoneForm studentQuizDoneForm) {
        
    }
}
