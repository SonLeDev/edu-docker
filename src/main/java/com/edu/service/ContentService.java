package com.edu.service;

import com.edu.domain.*;
import com.edu.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by sstvn on 5/2/17.
 */
@Service
public class ContentService {

    @Autowired
    EduClassRepository eduClassRepository;
    @Autowired
    EduSubjectRepository eduSubjectRepository;

    @Autowired
    ExaminationPapperRepository examinationPapperRepository;

    @Autowired
    PracticePapperRepository practicePapperRepository;

    @Autowired
    EduExamQuizRepository eduExamQuizRepository;

    @Autowired EduChapterRepository eduChapterRepository;

    public Collection<String> getClassLst(){
        Collection<String> classLst = new HashSet<>();
        eduClassRepository.findAll().forEach(e->{classLst.add(e.getClassName());});
        return classLst;
    }

    public List<EduSubject> getSubjectLst(String eduClassName) {
        return eduSubjectRepository.findByClassName(eduClassName);
    }

    public Collection<ExaminationPapper> getExamLst(Long classId, Long subjectId) {
//        return null;
        return examinationPapperRepository.findByClassIdAndSubjectId(classId,subjectId);
    }

    public Collection<PracticePapper> getChapterLst(String classId, String subjectId) {
        return practicePapperRepository.findAll();
    }

    public Collection<EduExamQuiz> getExamQuizList(Long examId) {
        return eduExamQuizRepository.findAll();
    }


    /*

        [{
            id:1
            subjectName:Toan
            ChapterList:[{chapterNo:1,chapterName:Do thi ham so},{...}]
        },
        {...}
        ]
     */
    public List<Map> getSubjectChapters(String classId) {

        List<EduSubject> eduSubjects = eduSubjectRepository.findByClassName(classId);
        List<Map> subjectChapters = new ArrayList<>();
        for(EduSubject eduSubject : eduSubjects){
            Map<String,Object> subObj = new HashMap<>();
            subjectChapters.add(subObj);

            subObj.put("id",eduSubject.getId());
            subObj.put("subjectName",eduSubject.getSubjectName());
            List<Map<String,Object>> eduChapters = new ArrayList<>();
            subObj.put("eduChapters",eduChapters);
            Set<Integer> existChapterNo = new HashSet<>();
            for(EduChapter chapter : eduSubject.getEduChapters()){
                if(existChapterNo.add(chapter.getChapterNo())){
                    Map<String,Object> chpObj = new HashMap<>();
                    chpObj.put("chapterNo",chapter.getChapterNo());
                    chpObj.put("chapterName",chapter.getChapterName());
                    eduChapters.add(chpObj);
                }
            }
        }
        return subjectChapters;
    }

    public List<EduChapter> getChapterDetail(String subjectId, String chapterNo) {
        return eduChapterRepository.findBySubjectIdAndChapterNo(Integer.valueOf(subjectId),
                                                            Integer.valueOf(chapterNo));
    }
}
