package com.edu.service;

import com.edu.common.SecurityUtils;
import com.edu.domain.Profile;
import com.edu.form.ProfileLoginArg;
import com.edu.form.ProfileRegisteredForm;
import com.edu.form.RegistrationForm;
import com.edu.pattern.ProfileBuilder;
import com.edu.repository.EduClassRepository;
import com.edu.repository.ProfileRepository;
import com.edu.repository.ProfileSignInRepository;
import com.edu.repository.StudentClassRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sstvn on 5/1/17.
 */
@Service
public class LoginService {

    @Autowired
    ProfileRepository profileRepository;
    @Autowired
    StudentClassRepository studentClassRepository;
    @Autowired
    EduClassRepository eduClassRepository;

    @Autowired
    ProfileSignInRepository profileSignInRepository;


    //@Transactional
    public ProfileRegisteredForm registrationAccessing(RegistrationForm registrationForm) {

        ProfileRegisteredForm regForm = new ProfileRegisteredForm();
        Profile profile = null;
        if(StringUtils.isNotEmpty(registrationForm.getFbTocken())){
            profile =   profileRepository.findByFacebookId(registrationForm.getFbTocken());
        }else if(StringUtils.isNotEmpty(registrationForm.getGgToken())){
            profile = profileRepository.findByGoogleId(registrationForm.getGgToken());
        }
        ProfileBuilder profileBuilder  = ProfileBuilder.newInstance();;
        if(profile == null){
            // register become a user of application
            profile = profileRepository
                    .save(profileBuilder
                            .setUserName(registrationForm.getUserName())
                            .setFbToken(registrationForm.getFbTocken())
                            .setGGToken(registrationForm.getGgToken())
                            .register());
        }

        profileBuilder.updateProfile(profile);

        // issue accessToken API for this user
        String accessToken = SecurityUtils.generateToken();
        profileSignInRepository.save(profileBuilder.signIn(accessToken,registrationForm.getDeviceId()));

        // take part into a class, become a StudentClass
        studentClassRepository.save(profileBuilder.becomeStudent(registrationForm.getClassName()));

        regForm.setProfile(profile);
        regForm.setAccessToken(accessToken);
        regForm.setClassName(registrationForm.getClassName());

        return regForm;
    }

    public void updateFBProfile(ProfileLoginArg profileLoginArg, String fbToken) {
       Profile profile = profileRepository.findOne(profileLoginArg.getUserId());
       if(profile!=null){
           profile.setFacebookId(fbToken);
           profileRepository.saveAndFlush(profile);
       }
    }

    public void updateGGProfile(ProfileLoginArg profileLoginArg, String ggToken) {
        Profile profile = profileRepository.findOne(profileLoginArg.getUserId());
        if(profile!=null){
            profile.setGoogleId(ggToken);
            profileRepository.saveAndFlush(profile);
        }
    }
}
