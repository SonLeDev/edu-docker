package com.edu.controller;

import com.edu.form.ProfileLoginArg;
import com.edu.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

/**
 * Created by sstvn on 4/29/17.
 */
@RestController
@RequestMapping("/content")

public class ContentController {

    @Autowired
    ContentService contentService;


    @RequestMapping("/classList")
    public Collection<String> getClassLst(){
        return contentService.getClassLst();
    }

    @RequestMapping(value = "/class/{classId}/subjectList",method = RequestMethod.GET)
    public ResponseEntity getSubjectLst(
            ProfileLoginArg profileLoginArg,
            @PathVariable("classId") String classId){
        //TODO : suject include chapter inside its

        ResponseEntity resEntity = new ResponseEntity(
                contentService.getSubjectLst(classId),
                HttpStatus.OK);

        return resEntity;

    }
    @RequestMapping(value = "/class/{classId}/subjectChapters",method = RequestMethod.GET)
    public ResponseEntity getSubjectChapters(
            ProfileLoginArg profileLoginArg,
            @PathVariable("classId") String classId){
        //TODO : suject include chapter inside its

        ResponseEntity resEntity = new ResponseEntity(
                contentService.getSubjectChapters(classId),
                HttpStatus.OK);

        return resEntity;

    }

    @RequestMapping("class/{classId}/subject/{subjectId}/examList")
    public ResponseEntity getExaminationList(ProfileLoginArg profileLoginArg,
            @PathVariable("classId") Long classId,
            @PathVariable("subjectId") Long subjectId){

        ResponseEntity resEntity = new ResponseEntity(
                contentService.getExamLst(classId,subjectId),
                HttpStatus.OK);
        return resEntity;
    }

    @RequestMapping("class/{classId}/subject/{subjectId}/chapterList")
    public ResponseEntity getChapterList(@PathVariable("classId") String classId,
                                         @PathVariable("subjectId") String subjectId){
        ResponseEntity resEntity = new ResponseEntity(
                contentService.getChapterLst(classId,subjectId),
                HttpStatus.OK);
        return resEntity;
    }
    @RequestMapping("/subject/{subjectId}/chapter/{chapterNo}")
    public ResponseEntity getChapterDetails(@PathVariable("subjectId") String subjectId,
                                            @PathVariable("chapterNo") String chapterNo){
        ResponseEntity resEntity = new ResponseEntity(
                contentService.getChapterDetail(subjectId,chapterNo),
                HttpStatus.OK);
        return resEntity;
    }


}

