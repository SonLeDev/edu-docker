package com.edu.controller;

import com.edu.form.ProfileLoginArg;
import com.edu.form.RegistrationForm;
import com.edu.form.ResponseLoginForm;
import com.edu.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by SonLee on 4/25/2017.
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    LoginService loginService;

    @RequestMapping("/a")
    public String login(){
        return "you are in login page";
    }

    @RequestMapping(value = "/registration",method = RequestMethod.POST)
        public ResponseEntity firstLogin(
                @RequestBody RegistrationForm registrationForm){

        ResponseEntity resEntity = new ResponseEntity(
                loginService.registrationAccessing(registrationForm),
                HttpStatus.OK);

        return resEntity;
    }

    @RequestMapping(value = "/linkFacebook",method = RequestMethod.POST)
    public Boolean linkFacebook(ProfileLoginArg profileLoginArg,
                                @RequestParam("fbToken") String fbToken){
        loginService.updateFBProfile(profileLoginArg,fbToken);
        return true;
    }

    @RequestMapping(value = "/linkGoogle",method = RequestMethod.POST)
    public Boolean linkGoogle(ProfileLoginArg profileLoginArg,
                                @RequestParam("ggToken") String ggToken){
        loginService.updateGGProfile(profileLoginArg,ggToken);
        return true;
    }

    @RequestMapping(value = "/facebookLogin")
    public ResponseEntity facebookLogin(@RequestBody RegistrationForm registrationForm){
        ResponseEntity resEntity = new ResponseEntity(
                loginService.registrationAccessing(registrationForm),
                HttpStatus.OK);
        return resEntity;
    }

    @RequestMapping("/googleLogin")
    public ResponseEntity googleLogin(@RequestBody RegistrationForm registrationForm){
        ResponseEntity resEntity = new ResponseEntity(
                loginService.registrationAccessing(registrationForm),
                HttpStatus.OK);
        return resEntity;
    }




}
