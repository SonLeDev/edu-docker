package com.edu.controller;

import com.edu.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sstvn on 4/29/17.
 */
@RestController
@RequestMapping("/content/exam")

public class ExaminationContentController {

    @Autowired
    ContentService contentService;

    @RequestMapping("class/{classId}/subject/{subjectId}/list")
    public ResponseEntity getExaminationList(@PathVariable("classId") Long classId,
                                                @PathVariable("subjectId") Long subjectId){

        ResponseEntity resEntity = new ResponseEntity(
                contentService.getExamLst(classId,subjectId),
                HttpStatus.OK);
        return resEntity;
    }

    @RequestMapping("{examId}/quizList")
    public ResponseEntity getQuizList(@PathVariable("examId") Long examId){

        ResponseEntity resEntity = new ResponseEntity(
                contentService.getExamQuizList(examId),
                HttpStatus.OK);
        return resEntity;

    }


}

