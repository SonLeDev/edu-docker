package com.edu.controller;

import com.edu.form.ProfileLoginArg;
import com.edu.form.StudentQuizDoneForm;
import com.edu.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sstvn on 5/2/17.
 */
@RestController
@RequestMapping("/student")
public class StudentController {

    @RequestMapping("testNow")
    public String testNow(ProfileLoginArg profileLoginArg){
        return "";
    }
    /*
        /student/exam/{examId}
        /student/exam/{examId}/
        /student/exam/{examId}/review
        /student/practice/chapter/{chapterId}
        /student/practice/chapter/{chapterId}/finished
        /student/practice/chapter/{chapterId}/review
     */

    @Autowired
    StudentService studentService;

    @RequestMapping("/exam/{examId}")
    public ResponseEntity studentTakeExamination(ProfileLoginArg profileLoginArg,
                                         @PathVariable("examId") Long examId){

        ResponseEntity resEntity = new ResponseEntity(
                studentService.takeExamination(examId,profileLoginArg.getUserId()),
                HttpStatus.OK);
        return resEntity;

    }

    @RequestMapping("/exam/done/session/{examSessionId}")
    public String student(ProfileLoginArg profileLogin,
                          @PathVariable("examSessionId") Long examSessionId,
                            @RequestBody StudentQuizDoneForm studentQuizDoneForm){

        studentQuizDoneForm.setSessionId(examSessionId);
        studentQuizDoneForm.setUserId(profileLogin.getUserId());
        studentService.examDone(studentQuizDoneForm);
        //TODO : should be split into another thread, don't stop the request flow
        studentService.analyseResult(studentQuizDoneForm);
        return "";
    }



}
