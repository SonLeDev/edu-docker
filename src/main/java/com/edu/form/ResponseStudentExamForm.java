package com.edu.form;

import com.edu.domain.EduAnswerQuiz;
import com.edu.domain.EduExamQuiz;

import java.util.List;
import java.util.Map;

/**
 * Created by SonLee on 5/3/2017.
 */
public class ResponseStudentExamForm {
    private Long examId;
    private Long userId;
    private List<EduExamQuiz> quizList;
    private Map<Long,EduAnswerQuiz> mapQuizAnswer;


}
