package com.edu.form;

/**
 * Created by sstvn on 5/11/17.
 */
public class StudentQuizDoneForm {
    private Long sessionId;
    private Long userId;
    private Long examId;
    private String quizAnswer; // 1A2B3CD5E

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public String getQuizAnswer() {
        return quizAnswer;
    }

    public void setQuizAnswer(String quizAnswer) {
        this.quizAnswer = quizAnswer;
    }
}
