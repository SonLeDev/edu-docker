package com.edu.form;

import com.edu.domain.Profile;

/**
 * Created by sstvn on 5/2/17.
 */
public class ProfileRegisteredForm {
    private Profile profile;
    private String accessToken;
    private String className;


    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
