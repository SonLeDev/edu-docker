package com.edu.form;

/**
 * Created by sstvn on 5/2/17.
 */
public class RegistrationForm {

    private String userName ="";
    private String className;
    private String deviceId;
    private String fbTocken = "";
    private String ggToken = "";

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getFbTocken() {
        return fbTocken;
    }

    public void setFbTocken(String fbTocken) {
        this.fbTocken = fbTocken;
    }

    public String getGgToken() {
        return ggToken;
    }

    public void setGgToken(String ggToken) {
        this.ggToken = ggToken;
    }
}
