package com.edu.form;

/**
 * Created by sstvn on 5/2/17.
 */
public class SocialAccountLoginForm {

    private String fbToken;
    private String ggToken;

    public String getFbToken() {
        return fbToken;
    }

    public void setFbToken(String fbToken) {
        this.fbToken = fbToken;
    }

    public String getGgToken() {
        return ggToken;
    }

    public void setGgToken(String ggToken) {
        this.ggToken = ggToken;
    }
}
