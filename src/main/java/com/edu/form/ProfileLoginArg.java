package com.edu.form;

/**
 * Created by sstvn on 5/2/17.
 */
public class ProfileLoginArg {
    private Long userId;
    private String deviceId;
    private String accessToken;

    public ProfileLoginArg(Long userId,String deviceId, String accessToken) {
        this.userId = userId;
        this.accessToken = accessToken;
        this.deviceId = deviceId;

    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }


    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


}
