package com.edu;

import com.edu.common.ProfileLoginHandlerMethodArgumentResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/**
 * Created by SonLee on 4/25/2017.
 */
@Configuration
@EnableWebMvc
public class SpringConfiguration extends WebMvcConfigurerAdapter {

    @Autowired ProfileLoginHandlerMethodArgumentResolver profileLoginHandlerMethodArgumentResolver;
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(profileLoginHandlerMethodArgumentResolver);
    }
}
