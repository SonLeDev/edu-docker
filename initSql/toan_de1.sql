INSERT INTO edu_quiz(no_of_quest,question,answera, answerb, answerc, answerd, correct_answer, level,chapterId, solution,detail_answer )
VALUES
  (1,'Đường thẳng nào sau đây là tiệm cận đứng của đồ thị hàm số \[y=\frac{2x+1}{x+1}\]?',
    '\[x=1\]','\[y=-1\]','\[y=2\]','\[x=-1\]','D',1,1,'','')
  ,(2,'Đồ thị của hàm số\[y={{x}^{4}}-2{{x}^{2}}+2\] và đồ thị hàm số \[y=-{{x}^{2}}+4\] có tất cả bao nhiêu điểm chung.'
      ,'0','4','1','2','D',2,1,'','Xét phương trình hoành độ giao điểm:\[{{x}^{4}}-2{{\text{x}}^{2}}+2=-{{x}^{2}}+4\]\[\Leftrightarrow {{x}^{4}}-{{x}^{2}}-2=0\Leftrightarrow \left( {{x}^{2}}+1 \right)\left( {{x}^{2}}-2 \right)=0\Leftrightarrow x=\pm \sqrt{2}\]')
  ,(3,'Cho hàm số \[y=f(x)\] xác định và liên tục trên đoạn \[\left[ -2;2 \right]\] và có đồ thị là đường cong trong hình vẽ bên. Hàm số \[f(x)\] đạt cực đại tại điểm nào sau đây?'
      ,'\[x=-2\]','\[x=-1\]','\[x=1\]','\[x=2\]','D',2,2,'','Tại \[x=-1\] thì y lớn hơn các giá trị xung quanh nó, chú ý: tại \[x=2\] và \[x=-2\]thì y đạtGTLN, GTNN chứ không phải cực trị.')
  ,(4,'Cho hàm số \[y={{x}^{3}}-2{{x}^{2}}+x+1\]. Mệnh đề nào dưới đây đúng?'
      ,'Hàm số nghịch biến trên khoảng\[\left( \frac{1}{3};1 \right)\].'
      ,'Hàm số nghịch biến trên khoảng\[\left( -\infty ;\frac{1}{3} \right)\].'
      ,'Hàm số đồng biến trên khoảng\[\left( \frac{1}{3};1 \right)\].'
      ,'Hàm số nghịch biến trên khoảng\[\left( 1;+\infty  \right)\].'
      ,'A',2,3,'Sử dụng định nghĩa','\[y''=3{{x}^{2}}-4x+1=\left( x-1 \right)\left( 3x-1 \right)\to y''<0\]khi\[\frac{1}{3}<x<1\]nên y nghịch biến trên\[\left( \frac{1}{3};1 \right)\]')
  ,(5,'Cho hàm số \[y=f(x)\] xác định trên \[R\backslash \left\{ 0 \right\}\], liên tục trên mỗi khoảng xác định và có bảng biến thiên như sau ... Tìm tập hợp tất cả các giá trị của tham số m sao cho phương trình \[f(x)=m\] có ba nghiệm thực phân biệt?'
      ,'\[\left[ -1;2 \right]\]','\[\left( -1;2 \right)\]	','\[(-1;2\text{ }\!\!]\!\!\text{ }\]','\[(-\infty ;2]\]','B',2,3
      ,'Dựa vào tính chất liên tục của đồ thị hàm số'
      ,'Dựa vào bảng biến thiên ta dễ thấy đường thẳng\[y=m\]cắt đồ thị tại 3 điểm phân biệt \[\Leftrightarrow -1<m<2\]')
  ,(6,'Cho hàm số \[y=\frac{{{x}^{2}}+3}{x+1}\]. Mệnh đề nào dưới đây đúng?'
      ,'Cực tiểu của hàm số bằng −3.','Cực tiểu của hàm số bằng 1.','Cực tiểu của hàm số bằng −6.','Cực tiểu của hàm số bằng 2.'
      ,'D',2,4,'Sử dụng định nghĩa, tính chất cực trị','TXĐ: \[D=\mathbb{R}\backslash \left\{ 1 \right\}\] Ta có \[y''=\frac{{{x}^{2}}+2x-3}{{{\left( x+1 \right)}^{2}}}\to y''=0\Leftrightarrow x=-3\] hoặc \[x=1\] \n
        Xét y trên một khoảng chứa 1 (lân cận của 1) là (0,2) ta thấy trên khoảng này thì lập BBT từ BBT suy ra tại x = 1 thì y nhỏ hơn các giá trị của y tại các giá trị của x trong lân cận của 1 D đó, \[x=1\] là điểm cực tiểu của hàm số, lại có\[y\left( 1 \right)=2\]nên 2 là cực tiểu của hs')
  ,(7,'Một vật chuyển động theo quy luật $s=-\frac{1}{3}{{t}^{\text{3}}}\text{+9}{{t}^{\text{2}}},$với t (giây) là khoảng thời gian tính từ lúc vật bắt đầu chuyển động và s (mét) là quãng đường vật đi được trong thời gian đó. Hỏi trong khoảng thời gian 10 giây, kể từ lúc bắt đầu chuyển động, vận tốc lớn nhất của vật đạt được bằng bao nhiêu ?'
      ,'216 (m/s).','30 (m/s).','400 (m/s).','54  (m/s).','D',3,5,'Vận dụng tính chất: Vận tốc là đạo hàm của phương trình chuyển động'
      ,'Ta có\[v=s''=\frac{-3}{2}{{t}^{2}}+18t\] \n
        Do cần tìm\[{{v}_{\max }}\] trong 10 giây đầu tiên nên cần tìm GTLN của\[v\left( t \right)=-\frac{3}{2}{{t}^{2}}+18t\] trên \[\left[ 0;10 \right]\] \n
        Có \[v''\left( t \right)=-3t+18\to v''\left( t \right)=0\Leftrightarrow t=6\] \n
        Do v(t) liên tục và\[v\left( 0 \right)=0,v\left( 10 \right)=30,v\left( 6 \right)=54\] do đó \[{{v}_{\max }}=54m/s\]')
  ,(8,'Tìm tất cả các tiệm cận đứng của đồ thị hàm số $y=\frac{2x-1-\sqrt{{{x}^{2}}+x+3}}{{{x}^{2}}-5x+6}.$'
      ,'$x=-3.$và$x=-2.$','$x=-3.$','$x=3.$và$x=2.$','$x=3.$','D',3,6,'','Ta có: \[{{x}^{2}}-5x+6=0\Leftrightarrow x=2\] hoặc \[x=3\] \n
\[\underset{x\to {{3}^{+}}}{\mathop{\lim }}\,\frac{2x-1-\sqrt{{{x}^{2}}+x+3}}{{{x}^{2}}-5x+6}=\underset{x\to {{3}^{+}}}{\mathop{\lim }}\,\frac{3x+1}{\left( x-3 \right)\left( 2x-1+\sqrt{{{x}^{2}}+x+3} \right)}=\frac{10}{5+\sqrt{15}}\underset{x\to {{3}^{+}}}{\mathop{\lim }}\,\frac{1}{\left( x-3 \right)}=+\infty \]
\[\underset{x\to 2}{\mathop{\lim }}\,\frac{2x-1-\sqrt{{{x}^{2}}+x+3}}{{{x}^{2}}-5x+6}=\underset{x\to 2}{\mathop{\lim }}\,\frac{4{{x}^{2}}-4x+1-{{x}^{2}}-x-3}{\left( x-2 \right)\left( x-3 \right)\left( 2x-1+\sqrt{{{x}^{2}}+x+3} \right)}=\underset{x\to 2}{\mathop{\lim }}\,\frac{\left( 3x+1 \right)\left( x-2 \right)}{\left( x-2 \right)\left( x-3 \right)\left( 2x-1+\sqrt{{{x}^{2}}+x+3} \right)}\]= \[\underset{x\to 2}{\mathop{\lim }}\,\frac{3x+1}{\left( x-3 \right)\left( 2x-1+\sqrt{{{x}^{2}}+x+3} \right)}=-\frac{7}{6}\]
Do đó chỉ có \[x=3\] là tiệm cân đứng của đồ thị hàm số.')
  ,(9,'Tìm tập hợp tất cả các giá trị của tham số thực m để hàm số $y=\ln ({{x}^{2}}+1)-mx\text{+1}$đồng biến trên khoảng $(-\infty ;+\infty ).$'
      ,'$(-\infty ;-1].$','$\int{f(x)dx}=-\frac{1}{2}\text{sin}\,\text{2x}\,\text{+}\,\text{C}$','$S=\,-2$','$\text{ }\!\![\!\!\text{ 1;+}\infty ).$','A',2,4,'Sử dụng định nghĩa'
      ,'\[y''=\frac{2x}{{{x}^{2}}+1}-m\to y''\ge 0\]với mọi x \[\Leftrightarrow m\le \frac{2x}{{{x}^{2}}+1}\] với mọi x hay \[m\le \min \frac{2x}{{{x}^{2}}+1}\] \n
Do \[\frac{2x}{{{x}^{2}}+1}\ge -1,\forall x\] dấu bằng xảy ra khi và chỉ khi \[x=-1\] nên \[m\le -1\]là tất cả giá tị cần tìm')
  ,(10,'Biết  $M(0;2),\text{ N(2;-2)}$ là các điểm cực trị của đồ thị hàm số $y=a{{x}^{3}}+b{{x}^{\text{2}}}\text{+c}x\text{+}d.$ Tính giá trị của hàm số tại $x=-2.$'
      ,'$y(-2)=2.$','$y(-2)=22.$','$\frac{1}{2}<\left| z \right|<\frac{3}{2}.$','$y(-2)=-18.$','D',2,5,'Sử dụng định nghĩa','\[y''=3a{{x}^{2}}+2bx+c\] \n
Do \[M\left( 0;2 \right)\] và \[N\left( 2;-2 \right)\]là các điểm cực trị của đths nên\[y''\left( 0 \right)=0\] và \[y''\left( 2 \right)=0\] hay \[c=0\]và \[12a+4b=0\]. \n
M,N thuộc đồ thị hàm số nên:\[y\left( 0 \right)=2\] và \[y\left( 2 \right)=-2\] hay \[d=2\] và \[8a+4b+2c+d=-2\to 8a+4b=-4\] \n
Từ đó suy ra \[a=1\] và \[b=-3\to y\left( -2 \right)=-18\]')
  ,(11,'Cho hàm số \[y=a{{x}^{3}}+b{{x}^{2}}+cx+d\] có đồ thị như  hình vẽ bên. Mệnh đề nào dưới đây đúng?'
      ,'\[a<0,b>0,c>0,d<0\].','\[a<0,b<0,c>0,d<0\].','\[a>0,b<0,c<0,d>0\].','\[a<0,b>0,c<0,d<0\].','A',3,4,'Sử dụng định nghĩa'
      ,'Do khi x đến dương vô cùng thì y đến âm vô cùng nên a âm \n
        đồ thị cắt Oy tại điểm có tung độ âm nên d âm \n
        \[y''=3a{{x}^{2}}+2bx+c\] \n
        từ đồ thị hàm số suy ra 2 điểm cực trị của hàm số có một điểm âm và một điểm dương trong đó điểm dương xa O hơn điểm âm tức là có trị tuyệt đối lớn hơn. Gọi  2 điểm này là \[{{x}_{1}},{{x}_{2}}\]. Ta có \[{{x}_{1}}{{x}_{1}}<0\]và \[{{x}_{1}}+{{x}_{2}}>0\]. Theo định lý Viet:\[{{x}_{1}}{{x}_{2}}=\frac{c}{3a}\] và \[{{x}_{1}}+{{x}_{2}}=\frac{-2b}{3a}\] lại có a âm nên \[c>0\], \[b>0\].')
  ,(12,'Với các số thực dương a, b bất kì. Mệnh đề nào dưới đây đúng ?'
      ,'$\ln (ab)=\ln a+\ln b.$','$\ln (ab)=\ln a.\ln b.$','$\ln \frac{a}{b}=\frac{\ln a}{\ln b}.$','$\ln \frac{a}{b}=\ln b-\ln a.$'
      ,'A',1,4,'Sử dụng định nghĩa, tính chất logarit','')
  ,(13,'Tìm các nghiệm của phương trình${{3}^{x-1}}=27.$'
      ,'$x=9.$','$x=3.$','$x=4.$','$x=10.$','C',1,2,'Tính đơn điệu','\[x-1=3\Leftrightarrow x=4\]')
  ,(14,'Số lượng của loại vi khuẩn A trong một phòng thí nghiệm được tính theo công thức $s(t)=s(0){{.2}^{t}},$ trong đó $s(0)$ là số lượng vi khuẩn A  lúc ban đầu, $s(t)$ là số lượng vi khuẩn A  có saut (phút). Biết sau 3 phút thì số lượng vi khuẩn A  là 625 nghìn con. Hỏi sau bao lâu, kể từ lúc bắt đầu, số lượng vi khuẩn A  là 10 triệu con ?'
      ,'48phút.','19phút.','7phút.','12 phút.','C',2,5,'Sử dụng định nghĩa','Theo giả thiết\[\Rightarrow 62500=s\left( 0 \right){{.2}^{3}}\to s\left( 0 \right)=\frac{625000}{8}\] \n
        khi số vi khuẩn là 10 triệu con thì\[{{10}^{7}}=s\left( 0 \right){{.2}^{t}}\Rightarrow {{2}^{t}}=128\Rightarrow t=7\] (phút)')
  ,(15,'Cho biểu thức  $P=\sqrt[4]{x.\sqrt[3]{{{x}^{2}}.\sqrt{{{x}^{3}}}}},$với x>0. Mệnh đề nào dưới đây đúng ?'
      ,'$P={{x}^{\frac{1}{2}}}$','$P={{x}^{\frac{13}{24}}}$','$P={{x}^{\frac{1}{4}}}$','$P={{x}^{\frac{2}{3}}}$','B',1,5,'Sử dụng định nghĩa',
      ,'\[P=\sqrt[4]{x.\sqrt[3]{{{x}^{2}}.{{x}^{\frac{3}{2}}}}}=\sqrt[4]{x.\sqrt[3]{{{x}^{\frac{7}{2}}}}}=\sqrt[4]{x.{{x}^{\frac{7}{7}}}}=\sqrt[4]{{{x}^{\frac{13}{6}}}}={{x}^{\frac{13}{24}}}\]')
  ,(16,'Với các số thực dương a, b bất kì..Mệnh đề nào dưới đây đúng?'
      ,'${{\log }_{2}}\left( \frac{2{{a}^{3}}}{b} \right)=1+3{{\log }_{2}}a-{{\log }_{2}}b$'
      ,'${{\log }_{2}}\left( \frac{2{{a}^{3}}}{b} \right)=1+\frac{1}{3}{{\log }_{2}}a-{{\log }_{2}}b$'
      ,'${{\log }_{2}}\left( \frac{2{{a}^{3}}}{b} \right)=1+3{{\log }_{2}}a+{{\log }_{2}}b$'
      ,'${{\log }_{2}}\left( \frac{2{{a}^{3}}}{b} \right)=1+\frac{1}{3}{{\log }_{2}}a+{{\log }_{2}}b$'
      ,'A',2,5,'Sử dụng định nghĩa, tính chất logarit','')
  ,(17,'Tìm tập nghiệm S của bất phương trình  log_(1/2)⁡〖(x+1)<log_(1/2)⁡(2x-1) 〗'
      ,'$S=\left( 2;+\infty  \right)$','$S=\left( -\infty ;2 \right)$','$S=\left( \frac{1}{2};2 \right)$','$S=\left( -1;2 \right)$','C',2,6,'Sử dụng tính chất logarit',
      ,'ĐKXĐ:\[x>\frac{1}{2}\] \n
        Do \[0<\frac{1}{2}<1\] nên BPT \[\Leftrightarrow x+1>2x-1\]hay\[x<2\] \n
        Kết hợp điều kiện xác định suy ra\[\frac{1}{2}<x<2\]')
  ,(18,'Tính đạo hàm của hàm số y=ln⁡(1+√(x+1)).'
      ,'$y''=\frac{1}{2\sqrt{x+1}\left( 1+\sqrt{x+1} \right)}$','$y''=\frac{1}{1+\sqrt{x+1}}$','$y''=\frac{1}{\sqrt{x+1}\left( 1+\sqrt{x+1} \right)}$','$y''=\frac{2}{\sqrt{x+1}\left( 1+\sqrt{x+1} \right)}$'
      ,'A',2,1,'Sử dụng định nghĩa','\[y''=\frac{\frac{1}{2\sqrt{x+1}}}{1+\sqrt{x+1}}=\frac{1}{2\sqrt{x+1}\left( 1+\sqrt{x+1} \right)}\]')
  ,(19,'Cho ba số thực dương a, b, c khác 1. \n Đồ thị các hàm số \[y={{a}^{x}},\,\,y={{b}^{x}},\,\,y={{c}^{x}}\] được cho trong hình vẽ bên. Mệnh đề nào dưới đây đúng?'
      ,'\[a<b<c\].','\[a<c<b\].','\[b<c<a\].','\[c<a<b\].','B',3,4,'Sử dụng định nghĩa','Xét hàm\[y={{a}^{x}}\] với \[a>0\] và a khác 1. Ta có nếu\[a>1\]thì y đến dương vô cùng khi x đến dương vôcùng còn nếu a < 1 thì y dần về 0 khi x đến dương vô cùng
từ nhận xét trên và dựa vào đồ thị suy ra b,c >1 còn a <1
trên đồ thị, lấy một giá trị dương bất kỳ của x là α, ta thấy \[{{b}^{\alpha }}>{{c}^{\alpha }}\]. Xét hàm \[{{x}^{\alpha }}\]trên \[\left( 1;\infty  \right)\], có \[\left( {{x}^{\alpha }} \right)''=\alpha {{x}^{\alpha -1}}>0\]nên hàm đồng biến trên\[\left( 1;\infty  \right)\]. Do đó b > C  ')
  ,(20,'Tìm tập hợp tất cả các giá trị của tham số thực m để phương trình 6^x+(3-m) 2^x-m=0 có nghiệm thuộc khoảng $(0;1)$.'
      ,'[3;4].','[2;4].','(2:4).','(3:4).','C',2,4,'Đặt ẩn phụ hoặc rút m về dạng m = f(x)'
      ,'Phương trình tương đương:\[m=\frac{{{6}^{x}}+{{3.2}^{x}}}{{{2}^{x}}+1}\] \n
Xét \[f\left( x \right)=\frac{{{6}^{x}}+{{3.2}^{x}}}{{{2}^{x}}+1}\] trên \[\left( 0;1 \right)\]ta thấy f(x) liên tục và \n
\[f''\left( x \right)=\frac{{{6}^{x}}{{.2}^{x}}.\left( \ln 6-\ln 2 \right)+{{6}^{x}}.\ln 6+{{3.2}^{x}}.\ln 2}{{{\left( {{2}^{x}}+1 \right)}^{2}}}>0\]nên\[f\left( x \right)\] đồng biến. \n
Do đó \[f\left( x \right)>\underset{x\to 0}{\mathop{\lim }}\,f\left( x \right)=2\] và \[f\left( x \right)<\underset{x\to 1}{\mathop{\lim }}\,f\left( x \right)=4\] \n
Do đó \[2<m<4\] là giá trị cần tìm.')
  ,(21,'Xét các số thực a,b  thỏa mãn a>b>1. Tìm giá trị nhỏ nhất P_min của biểu thức$P=\log _{\frac{a}{b}}^{2}\left( {{a}^{2}} \right)+3{{\log }_{b}}\left( \frac{a}{b} \right)$'
      ,'${{P}_{\min }}=19$','${{P}_{\min }}=13$	C','${{P}_{\min }}=14$','${{P}_{\min }}=15$'
      ,'D',3,1,'Sử dụng định nghĩa'
      ,'\[P=\frac{1}{{{\left( {{\log }_{{{a}^{2}}}}\frac{a}{b} \right)}^{2}}}+3\left( {{\log }_{b}}a-1 \right)=\frac{4}{{{\left( 1-{{\log }_{a}}b \right)}^{2}}}+3\left( {{\log }_{b}}a-1 \right)\] \n
Đặt \[t={{\log }_{a}}b\] do \[a>b>1\] nên \[0<t<1\] \n
\[P=\frac{4}{{{\left( 1-t \right)}^{2}}}+\frac{3}{t}-3\] \n
Xét \[f\left( t \right)=\frac{4}{{{\left( 1-t \right)}^{2}}}+\frac{3}{t}-3\] trên \[\left( 0;1 \right)\]ta thấy GTNN của f(t) là\[f\left( \frac{1}{3} \right)=15\]')
  ,(22,'Tìm nguyên hàm của hàm số $f(x)=c\text{os}\,\text{2x}$.'
      ,'$\int{f(x)dx}=\frac{1}{2}\text{sin}\,\text{2x}\,\text{+}\,\text{C}$'
      ,'$\int{f(x)dx}=-\frac{1}{2}\text{sin}\,\text{2x}\,\text{+}\,\text{C}$'
      ,'$\int{f(x)dx}=2\text{sin}\,\text{2x}\,\text{+}\,\text{C}$'
      ,'$\int{f(x)dx}=-2\text{sin}\,\text{2x}\,\text{+}\,\text{C}$'
      ,'A',1,1,'Sử dụng định nghĩa','')
  ,(23,'Cho hàm số $f(x)$ có đạo hàm trên đoạn $\left[ 1;\,2 \right]$, $f(1)=\,1$  và $f(2)=\,\text{2}$. Tính $I=\int\limits_{1}^{2}{f''(x)dx}$.'
      ,'$I=\,1$','$I=\,-1$','$I=\,3$','$I=\,\frac{7}{2}$'
      ,'A',1,1,'Sử dụng định nghĩa, tính chất nguyên hàm'
      ,'Theo tính chất nguyên hàm, tích phân:\[I=f\left( 2 \right)-f\left( 1 \right)=1\]')
  ,(24,'Biết $F(x)$ là một nguyên hàm của của hàm số $f(x)=\,\frac{1}{x-1}$và $F(2)=\,1$. Tính $F(3)$'
      ,'$F(3)=\ln 2-1$','$F(3)=\ln 2+1$','$F(3)=\frac{1}{2}$','$F(3)=\frac{7}{4}$'
      ,'B',2,1,'Sử dụng định nghĩa','\[F\left( x \right)=\ln \left| x-1 \right|+C\] \n Ta có: \[F\left( 2 \right)=C=1\] do đó \[F\left( 3 \right)=\ln 2+1\]')
  ,(25,'Cho $\int\limits_{0}^{4}{f(x)dx}=16$. Tính $I=\int\limits_{0}^{2}{f(2x)dx}$'
      ,'$I=\,32$','$I=\,8$','$I=\,16$','$I=\,4$'
      ,'B',2,1,'Đổi biến tích phân','\[\int\limits_{0}^{2}{f\left( 2x \right)dx}=\frac{1}{2}.\int\limits_{0}^{4}{f\left( x \right)dx}\]	(đổi biến t = 2x) = 8')
  ,(26,'Biết $\int\limits_{3}^{4}{\frac{dx}{{{x}^{2}}+x}}=a\ln 2+b\ln 3+c\ln 5$, với a, b, c là các số nguyên. Tính $S=a+b+c$'
      ,'$S=6$','$S=\,2$','$S=\,-2$','$S=\,0$'
      ,'B',2,6,'Sử dụng tích phân hàm hữu tỷ','Ta có: \[{{2}^{a}}{{.3}^{b}}{{.5}^{c}}={{e}^{\int\limits_{3}^{4}{\frac{1}{{{x}^{2}}+x}dx}}}=\frac{16}{15}=\frac{{{2}^{4}}}{3.5}\to \left\{ \begin{align}& a=4 \\ & b=-1 \\ & c=-1 \\ \end{align} \right.\to S=2\]')
  ,(27,'Cho hình thang cong $(H)$giới hạnbới các đường $y={{e}^{x}},y=0,x=0$và $x=\ln 4$. Đường thẳng$x=k\,(0<k<\ln 4)$ chia $(H)$ thành hai phần có diện tích là ${{S}_{1}}$${{S}_{2}}$ và như hình vẽ bên. Tìm $x=k$để ${{S}_{1}}=2{{S}_{2}}$.'
      ,'$k=\frac{2}{3}\ln 4$','$k=\ln 2$','$k=\ln \frac{8}{3}$','$k=\ln 3$','D',2,1,'Sử dụng công thức tinhs diện tích hình phẳng'
      ,'Ta có: \[\left\{ \begin{align}& {{S}_{1}}=\int\limits_{0}^{k}{{{e}^{x}}} \\ & {{S}_{2}}=\int\limits_{0}^{\ln 4}{{{e}^{x}}-{{S}_{1}}=3-S{{ & }_{1}}=\frac{{{S}_{1}}}{2}} \\ \end{align} \right.\to \int\limits_{0}^{k}{{{e}^{x}}}=2\]\[\Rightarrow k=\ln 3\]')
  ,(28,'. Ông An có một mảnh vườn hình elip có độ dài trục lớn bằng 16m và độ dài trục bé bằng 10m. Ông muốn trồng hoa trên một dải đất rộng 8m và nhận trục bé của elip làm trục  đối xứng( như hình vẽ). Biết kinh phí để trồng hoa 100.000 đồng/1 m2. Hỏi Ông An cần bao nhiêu tiền để trồng hoa trên dải đất đó? ( Số tiền được làm tròn đến hàng nghìn)'
      ,'7.862.000 đồng','7.653.000 đồng','7.128.000 đồng','7.826.000 đồng','B',3,1,'Sử dụng định nghĩa, công thức tính diện tích'
      ,'Phương trình elip là:\[\frac{{{x}^{2}}}{64}+\frac{{{y}^{2}}}{25}=1\] . Ta có: diện tích mảnh vườn cần tìm được chia làm 2 qua trụclớn, gọi diện tích 1 phần là S. \n
Gắn tâm elip là O, trục lớn là Ox, trục bé là Oy. \n
Sử dụng ứng dụng tích phân, diện tích phần này sẽ giới hạn qua đường cong\[y=\sqrt{25-\frac{25{{x}^{2}}}{64}}\] và 2 đường \[x=4;x=-4\]. \n
Ta có: \[S=\int\limits_{-4}^{4}{\sqrt{25-\frac{25{{x}^{2}}}{64}}dx}=38,2644591\]( Sử dụng CASIO, tuy nhiên có thể giải thông thườngqua đặt\[x=8\sin t\]) \n
Như vậy số tiền cần có là:\[38,2644591.2.100000=7652891\approx 7653000\] ')
  ,(29,'Điểm M trong hình vẽ bên là điểm biểu diễn của số phức z. Tìm phần thực và phần ảo của số phức z.'
      ,'Phần thực là −4 và phần ảo là 3.','Phần thực là 3 và phần ảo là −4i.','Phần thực là 3 và phần ảo là −4.','Phần thực là −4 và phần ảo là 3i.	'
      ,'C',1,1,'Sử dụng biểu diễn số phức','Tọa độ\[M\left( 3;-4 \right)\] nên sẽ có phần thực là 3, phần ảo là -4( không phải là \[-4i\]).')
;