INSERT INTO edu_quiz(no_of_quest,question,answera, answerb, answerc, answerd)
VALUES
    (1,'Đường thẳng nào sau đây là tiệm cận đứng của đồ thị hàm số \[y=\frac{2x+1}{x+1}\]?','\[x=1\]','\[y=-1\]','\[y=2\]','\[x=-1\]')
  ,(2,'Đồ thị của hàm số \[y={{x}^{4}}-2{{x}^{2}}+2\] và đồ thị hàm số \[y=-{{x}^{2}}+4\] có tất cả bao nhiêu điểm chung.','0','4','1','2')
  ,(3,'Cho hàm số \[y=f(x)\] xác định và liên tục trên đoạn \[\left[ -2;2 \right]\] và có đồ thị là đường cong trong hình vẽ bên. Hàm số \[f(x)\] đạt cực đại tại điểm nào sau đây?'
          ,'\[x=-2\]','\[x=-1\]','\[x=1\]','\[x=2\]')
  ,(4,'Cho hàm số \[y={{x}^{3}}-2{{x}^{2}}+x+1\]. Mệnh đề nào dưới đây đúng?','Hàm số nghịch biến trên khoảng\[\left( \frac{1}{3};1 \right)\].'
          ,'Hàm số nghịch biến trên khoảng\[\left( -\infty ;\frac{1}{3} \right)\].'
          ,'Hàm số đồng biến trên khoảng\[\left( \frac{1}{3};1 \right)\].','Hàm số nghịch biến trên khoảng\[\left( 1;+\infty  \right)\].')
  ,(5,'Cho hàm số \[y=f(x)\] xác định trên \[R\backslash \left\{ 0 \right\}\], liên tục trên mỗi khoảng xác định và có bảng biến thiên như sau, ..., Tìm tập hợp tất cả các giá trị của tham số m sao cho phương trình \[f(x)=m\] có ba nghiệm thực phân biệt?'
          ,' \[\left[ -1;2 \right]\]','\[\left( -1;2 \right)\]','\[(-1;2\text{ }\!\!]\!\!\text{ }\]','\[(-\infty ;2]\]')
  ,(6,'Cho hàm số \[y=\frac{{{x}^{2}}+3}{x+1}\]. Mệnh đề nào dưới đây đúng?'
          ,'Cực tiểu của hàm số bằng −3.','Cực tiểu của hàm số bằng 1.','Cực tiểu của hàm số bằng −6.','Cực tiểu của hàm số bằng 2.')
  ,(7,'Một vật chuyển động theo quy luật $s=-\frac{1}{3}{{t}^{\text{3}}}\text{+9}{{t}^{\text{2}}},$ với t (giây) là khoảng thời gian tính từ lúc vật bắt đầu chuyển động và s (mét) là quãng đường vật đi được trong thời gian đó. Hỏi trong khoảng thời gian 10 giây, kể từ lúc bắt đầu chuyển động, vận tốc lớn nhất của vật đạt được bằng bao nhiêu ?'
          ,'216 (m/s).','30 (m/s).','400 (m/s).','54 (m/s).')
  ,(8,'Tìm tất cả các tiệm cận đứng của đồ thị hàm số  $y=\frac{2x-1-\sqrt{{{x}^{2}}+x+3}}{{{x}^{2}}-5x+6}.$'
          ,'$x=-3.$ và $x=-2.$','$x=-3.$','$x=3.$','$x=3.$')
  ,(9,'Tìm tập hợp tất cả các giá trị của tham số thực m để hàm số $y=\ln ({{x}^{2}}+1)-mx\text{+1}$ đồng biến trên khoảng  $(-\infty ;+\infty ).$'
          ,'$(-\infty ;-1].$','$\int{f(x)dx}=-\frac{1}{2}\text{sin}\,\text{2x}\,\text{+}\,\text{C}$','$S=\,-2$','$\text{ }\!\![\!\!\text{ 1;+}\infty ).$')
  ,(10,'Biết  $M(0;2),\text{ N(2;-2)}$ là các điểm cực trị của đồ thị hàm số $y=a{{x}^{3}}+b{{x}^{\text{2}}}\text{+c}x\text{+}d.$ . Tính giá trị của hàm số tại $x=-2.$'
          ,'$y(-2)=2.$','$y(-2)=22.$','$\frac{1}{2}<\left| z \right|<\frac{3}{2}.$','$y(-2)=-18.$')
  ,(11,'Cho hàm số \[y=a{{x}^{3}}+b{{x}^{2}}+cx+d\] có đồ thị như  hình vẽ bên. Mệnh đề nào dưới đây đúng?'
          ,'\[a<0,b>0,c>0,d<0\].','\[a<0,b<0,c>0,d<0\].','\[a>0,b<0,c<0,d>0\].','\[a<0,b>0,c<0,d<0\].')
  ,(12,'Với các số thực dương a, b bất kì. Mệnh đề nào dưới đây đúng ?'
          ,'$\ln (ab)=\ln a+\ln b.$','$\ln (ab)=\ln a.\ln b.$','$\ln \frac{a}{b}=\frac{\ln a}{\ln b}.$','$\ln \frac{a}{b}=\ln b-\ln a.$')
  ,(13,'Tìm các nghiệm của phương trình ${{3}^{x-1}}=27.$'
          ,'$x=9.$','$x=3.$','$x=4.$','$x=10.$')
  ,(14,'Số lượng của loại vi khuẩn A trong một phòng thí nghiệm được tính theo công thức $s(t)=s(0){{.2}^{t}},$ trong đó $s(0)$ là số lượng vi khuẩn A  lúc ban đầu, $s(t)$ là số lượng vi khuẩn A  có sau  t (phút). Biết sau 3 phút thì số lượng vi khuẩn A  là 625 nghìn con. Hỏi sau bao lâu, kể từ lúc bắt đầu, số lượng vi khuẩn A  là 10 triệu con ?'
          ,'48 phút.','19 phút.','7 phút.','12 phút.')
  ,(15,'Cho biểu thức  $P=\sqrt[4]{x.\sqrt[3]{{{x}^{2}}.\sqrt{{{x}^{3}}}}},$với x>0. Mệnh đề nào dưới đây đúng ?'
          ,'$P={{x}^{\frac{1}{2}}}$','$P={{x}^{\frac{13}{24}}}$','$P={{x}^{\frac{1}{4}}}$','$P={{x}^{\frac{2}{3}}}$')
  ,(16,'Với các số thực dương a, b bất kì.. Mệnh đề nào dưới đây đúng?'
          ,'${{\log }_{2}}\left( \frac{2{{a}^{3}}}{b} \right)=1+3{{\log }_{2}}a-{{\log }_{2}}b$'
          ,'${{\log }_{2}}\left( \frac{2{{a}^{3}}}{b} \right)=1+\frac{1}{3}{{\log }_{2}}a-{{\log }_{2}}b$'
          ,'${{\log }_{2}}\left( \frac{2{{a}^{3}}}{b} \right)=1+3{{\log }_{2}}a+{{\log }_{2}}b$'
          ,'${{\log }_{2}}\left( \frac{2{{a}^{3}}}{b} \right)=1+\frac{1}{3}{{\log }_{2}}a+{{\log }_{2}}b$')
  ,(17,'Tìm tập nghiệm S của bất phương trình '
          ,'$S=\left( 2;+\infty  \right)$','$S=\left( -\infty ;2 \right)$','$S=\left( \frac{1}{2};2 \right)$','$S=\left( -1;2 \right)$')
  ,(18,'Tính đạo hàm của hàm số '
          ,'$y''=\frac{1}{2\sqrt{x+1}\left( 1+\sqrt{x+1} \right)}$'
          ,'$y''=\frac{1}{1+\sqrt{x+1}}$','$y''=\frac{1}{\sqrt{x+1}\left( 1+\sqrt{x+1} \right)}$'
          ,'$y''=\frac{2}{\sqrt{x+1}\left( 1+\sqrt{x+1} \right)}$')
  ,(19,'Cho ba số thực dương a, b, c khác 1. Đồ thị các hàm số \[y={{a}^{x}},\,\,y={{b}^{x}},\,\,y={{c}^{x}}\] được cho trong hình vẽ bên. Mệnh đề nào dưới đây đúng?'
          ,'\[a<b<c\].','\[a<c<b\].','\[b<c<a\].','\[c<a<b\].')
  ,(20,'Tìm tập hợp tất cả các giá trị của tham số thực m để phương trình 6^x+(3-m) 2^x-m=0 có nghiệm thuộc khoảng $(0;1)$.'
          ,'[3;4].','[2;4].','(2:4).','(3:4).')
  ,(21,'Xét các số thực a,b  thỏa mãn a > b > 1. Tìm giá trị nhỏ nhất P_min của biểu thức $P=\log _{\frac{a}{b}}^{2}\left( {{a}^{2}} \right)+3{{\log }_{b}}\left( \frac{a}{b} \right)$'
          ,'${{P}_{\min }}=19$','${{P}_{\min }}=13$','${{P}_{\min }}=14$','P_min=15')
  ,(22,'Tìm nguyên hàm của hàm số $f(x)=c\text{os}\,\text{2x}$.',
          ,'$\int{f(x)dx}=\frac{1}{2}\text{sin}\,\text{2x}\,\text{+}\,\text{C}$','$\int{f(x)dx}=-\frac{1}{2}\text{sin}\,\text{2x}\,\text{+}\,\text{C}$'
          ,'$\int{f(x)dx}=2\text{sin}\,\text{2x}\,\text{+}\,\text{C}$','$\int{f(x)dx}=-2\text{sin}\,\text{2x}\,\text{+}\,\text{C}$')
  ,(23,'Cho hàm số $f(x)$ có đạo hàm trên đoạn $\left[ 1;\,2 \right]$, $f(1)=\,1$  và $f(2)=\,\text{2}$. Tính $I=\int\limits_{1}^{2}{f''(x)dx}$.'
          ,'$I=\,1$','$I=\,-1$','$I=\,3$','$I=\,\frac{7}{2}$')
  ,(24,'Biết $F(x)$ là một nguyên hàm của của hàm số $f(x)=\,\frac{1}{x-1}$ và $F(2)=\,1$. Tính $F(3)$'
          ,'$F(3)=\ln 2-1$','$F(3)=\ln 2+1$','$F(3)=\frac{1}{2}$','$F(3)=\frac{7}{4}$')
  ,(25,'Cho $\int\limits_{0}^{4}{f(x)dx}=16$. Tính $I=\int\limits_{0}^{2}{f(2x)dx}$'
          ,'$I=\,32$','$I=\,8$','$I=\,16$','$I=\,4$')
  ,(26,'Biết $\int\limits_{3}^{4}{\frac{dx}{{{x}^{2}}+x}}=a\ln 2+b\ln 3+c\ln 5$, với a, b, c là các số nguyên. Tính $S=a+b+c$'
          ,'$S=6$','$S=\,2$','$S=\,-2$','$S=\,0$')
  ,(27,'Cho hình thang cong $(H)$giới hạn bới các đường $y={{e}^{x}},y=0,x=0$ và $x=\ln 4$. Đường thẳng $x=k\,(0<k<\ln 4)$ chia $(H)$ thành hai phần có diện tích là ${{S}_{1}}$ ${{S}_{2}}$ và như hình vẽ bên. Tìm $x=k$để ${{S}_{1}}=2{{S}_{2}}$.'
          ,'$k=\frac{2}{3}\ln 4$','$k=\ln 2$','$k=\ln \frac{8}{3}$','$k=\ln 3$')
  ,(28,'Ông An có một mảnh vườn hình elip có độ dài trục lớn bằng 16m và độ dài trục bé bằng 10m. Ông muốn trồng hoa trên một dải đất rộng 8m và nhận trục bé của elip làm trục đối xứng( như hình vẽ). Biết kinh phí để trồng hoa 100.000 đồng/1 m2. Hỏi Ông An cần bao nhiêu tiền để trồng hoa trên dải đất đó? ( Số tiền được làm tròn đến hàng nghìn)'
          ,'7.862.000 đồng','7.653.000 đồng','7.128.000 đồng','7.826.000 đồng')
  ,(29,'Điểm M trong hình vẽ bên là điểm biểu diễn của số phức z. Tìm phần thực và phần ảo của số phức z.'
          ,'Phần thực là −4 và phần ảo là 3.','Phần thực là 3 và phần ảo là −4i.','Phần thực là 3 và phần ảo là −4.','Phần thực là −4 và phần ảo là 3i.')
  ,(30,'Tìm số phức liên hợp của số phức $z=i(3i+1)$','$\overline{z}=3-i$','$\overline{z}=-3+i$','$\overline{z}=3+i$','$\overline{z}=-3-i$')
  ,(31,'Tính mô đun của số phức $z$thoả mãn $z(2-i)+13i=1.$','$\left| z \right|=\sqrt{34}.$','$\left| z \right|=34$','$\left|z\right|=\frac{5\sqrt{34}}{3}$','$\left| z \right|=\frac{\sqrt{34}}{3}$')
  ,(32,'Kí hiệu ${{z}_{0}}$là nghiệm phức có phần ảo dương của phương trình $4{{z}^{2}}-16z+17=0.$Trên mặt phẳng toạ độ, điểm nào dưới đây là điểm biểu diễn số phức \[\text{w}=i{{z}_{0}}?\]'
          ,'${{M}_{1}}\left( \frac{1}{2};2 \right).$','${{M}_{2}}\left( -\frac{1}{2};2 \right).$','${{M}_{3}}\left( -\frac{1}{4};1 \right).$','${{M}_{4}}\left( \frac{1}{4};1 \right).\,$')
  ,(33,'Cho số phức $z=a+bi(a,b\in R)$ thoả mãn $(1+i)z+2\overline{z}=3+2i.$Tính $P=a+b.$','$P=\frac{1}{2}$','$P=1$','$P=-1$','$P=-\frac{1}{2}$')
  ,(34,'Xét số phức $z$  thoả mãn $(1+2i)\left| z \right|=\frac{\sqrt{10}}{z}-2+i.$Mệnh đề nào sau đây đúng?','$\frac{3}{2}<z<2.$','$\left| z \right|>2.$','$\left| z \right|<\frac{1}{2}\,$','$\frac{1}{2}<\left| z \right|<\frac{3}{2}.$')
  ,(35,'Cho hình chóp $S.ABC$có đáy là tam giác đều cạnh $2a$ và thể tích bẳng ${{a}^{3}}.$Tính chiều cao $h$ của hình chóp đã cho.'
          ,'$h=\frac{\sqrt{3}a}{6}$','$h=\frac{\sqrt{3}a}{2}$','$h=\,\frac{\sqrt{3}a}{3}$','$h=\sqrt{3}a$')
  ,(36,'Hình đa diện nào dưới đây không có tâm đối xứng?'
          ,'Tứ diện đều','Bát diện đều','Hình lập phương','Lăng trụ lục giác đều')
  ,(37,'Cho tứ diện $ABCD$có thể tích bằng 12 và G là trọng tâm của tam giác BCD. Tính thể tích $V$của khối chóp $A.GBC$'
          ,'$V=3$','$V=\,\,4$','$V=6$','$V=5$')
  ,(38,'Cho hình lăng trụ tam giác  \[ABC.A''B''C''\] có đáy ABC là tam giác vuông cân tại A, cạnh $AC=2\sqrt{2}$. Biết $A{{C}^{''}}$tạo với mặt phẳng (ABC) một góc 600 và $A{{C}^{''}}=4$. Tính thể tích V của khối đa diện $ABC.{{A}^{''}}{{B}^{''}}{{C}^{''}}$.'
          ,'$V=\frac{8}{3}$','$V=\frac{16}{3}$','$V=\frac{8\sqrt{3}}{3}$','$V=\frac{16\sqrt{3}}{3}$')
  ,(39,'Cho khối nón (N) có bán kính đáy bằng 3 và diện tích xung quanh bằng $15\pi $. Tính thể tích V của khối nón (N).'
          ,'$V=12\pi $','$V=20\pi $','$V=36\pi $','$V=60\pi $')
  ,(40,'Cho hình lăng trụ tam giác đều \[ABC.A''B''C''\]có độ dài cạnh đáy bằng a và chiều cao bằng h. Tính thể tích V của khối trụ ngoại tiếp lăng trụ đã cho.'
          ,'$V=\frac{\pi {{a}^{2}}h}{9}$','$V=\frac{\pi {{a}^{2}}h}{3}$'
          ,'$V=3\pi {{a}^{2}}h$','$V=\pi {{a}^{2}}h$')
  ,(41,'Cho hình hộp chữ nhật \[ABCD.A''B''C''D''\] có \[AB=a,AD=2a,AA''=2a\]. Tính bán kính R của mặt cầu ngoại tiếp tứ diện \[ABB''C''\].'
          ,'$R=3a$','$R=\frac{3a}{4}$','$R=\frac{3a}{2}$','$R=2a$')
  ,(42,'Cho hai hình vuông cùng có cạnh bằng 5 được xếp chồng lên nhau sao cho đỉnh X của một hình vuông là tâm của hình vuông còn lại( như hình vẽ bên). Tính thể tích V của vật thể tròn xoay khi quay mô hình trên xung quanh trục XY .'
          ,'$V=\frac{125\left( 1+\sqrt{2} \right)\pi }{6}$','$V=\frac{125\left( 5+2\sqrt{2} \right)\pi }{12}$','$V=\frac{125\left( 5+4\sqrt{2} \right)\pi }{24}$','$V=\frac{125\left( 2+\sqrt{2} \right)\pi }{4}$')
  ,(43,'Trong không gian với hệ trục tọa độ Oxyz, cho hai điểm  . Tìm toạ độ trung điểm I  của đoạn thẳng  ?'
          ,'$I(-2;2;1).$','$I(1;0;4).$','$I(2;0;8).$','$I(2;-2;-1).$')
  ,(44,'Trong không gian với hệ trục tọa độ Oxyz, cho đường thẳng $d:\left\{ \begin{align} & x=1 \\ & y=2+3t\,\,\,\,(t\in R) \\ & z=5-t \\ \end{align} \right.$. Vectơ nào dưới đây là vectơ chỉ phương của $d$?'
          ,'$\,\overrightarrow{{{u}_{1}}}=\left( 0;3;-1 \right).$','$\,\overrightarrow{{{u}_{2}}}=\left( 1;3;-1 \right).$'
          ,'$\,\overrightarrow{{{u}_{3}}}=\left( 1;-3;-1 \right).$','$\,\overrightarrow{{{u}_{4}}}=\left( 1;2;5 \right).$')
  ,(45,'Trong không gian với hệ trục tọa độ Oxyz, cho ba điểm $A(1;0;0),\,\,B(0;-2;0)$ và $C(0;0;3)$. Phương trình nào dưới đây là phương trình của mặt phẳng $(ABC)$?')
;