CREATE DATABASE edu_ka_io
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `edu_quiz`;
CREATE TABLE `edu_quiz` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `answera` varchar(255) DEFAULT NULL,
  `answerb` varchar(255) DEFAULT NULL,
  `answerc` varchar(255) DEFAULT NULL,
  `answerd` varchar(255) DEFAULT NULL,
  `correct_answer` varchar(255) DEFAULT NULL,
  `detail_answer` varchar(255) DEFAULT NULL,
  `explain_answer` varchar(255) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `question` varchar(255) DEFAULT NULL,
  `solution` varchar(255) DEFAULT NULL,
  `no_of_quest` int(11) NOT NULL,
  `point_of_quiz` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO edu_ka_io.edu_class(id,class_name)
VALUE (10,'10'),(11,'11'),(12,'12'),(13,'13')
;

INSERT INTO edu_ka_io.edu_subject(class_name,subject_name)
VALUES ('12','Toán');

INSERT INTO edu_ka_io.edu_chapter(subject_id,chapter_no,chapter_name, code_unit, name_unit,order_unit)
VALUES  (1,1,'Khảo sát hàm số','S12.01','Ba bài toán về tiếp tuyến',01),
(1,1,'Khảo sát hàm số','S12.02','Tính đơn điệu',02),
(1,1,'Khảo sát hàm số','S12.03','Cực trị của hàm số',03),
(1,1,'Khảo sát hàm số','S12.04','Giá trị lớn nhất, nhỏ nhất',04),
(1,1,'Khảo sát hàm số','S12.05','Tiệm cận',05),
(1,1,'Khảo sát hàm số','S12.06','Đồ thị của hàm số',06),
(1,1,'Khảo sát hàm số','S12.07','Khảo sát hàm số bậc 3',07),
(1,1,'Khảo sát hàm số','S12.08','Khảo sát hàm số bậc 4 dạng trùng phương',08),
(1,1,'Khảo sát hàm số','S12.09','Khảo sát hàm số phân thức',09),
(1,2,'Hàm số lũy thừa. Hàm số mũ và hàm logarit','S12.10','Lũy thừa',10),
(1,2,'Hàm số lũy thừa. Hàm số mũ và hàm logarit','S12.11','Loga',11),
(1,2,'Hàm số lũy thừa. Hàm số mũ và hàm logarit','S12.12','Phương trình mũ',12),
(1,2,'Hàm số lũy thừa. Hàm số mũ và hàm logarit','S12.13','Bất phương trình mũ',13),
(1,2,'Hàm số lũy thừa. Hàm số mũ và hàm logarit','S12.14','Hệ phương trình mũ',14),
(1,2,'Hàm số lũy thừa. Hàm số mũ và hàm logarit','S12.15','Phương trình Loga',15),
(1,2,'Hàm số lũy thừa. Hàm số mũ và hàm logarit','S12.16','Bất phương trình Loga',16),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.17','Nguyên hàm',17),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.18','Tính nguyên hàm bằng định nghĩa',18),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.19','Tính nguyên hàm hàm vô tỷ',19),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.20','Tính nguyên hàm hàm phân thức',20),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.21','Tính nguyên hàm lượng giác',21),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.22','Tính nguyên hàm hàm bằng cách đổi biến',22),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.23','Tính nguyên hàm hàm từng phần',23),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.24','Tích phân',24),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.25','Tính tích phân bằng cách đổi biến',25),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.26','Tích phân hàm lượng giác',26),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.27','Tích phân hàm vô tỷ',27),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.28','Tích phân hàm phân thức',28),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.29','Tích phân từng phần',29),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.30','Tích phân hàm chẵn, lẻ, tuần hoàn (nâng cao)',30),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.31','Tíchphân hàm trị tuyệt đối',31),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.32','Ứng dụng tích phân tính diện tích',32),
(1,3,'Nguyên hàm, tích phân và ứng dụng','S12.33','Ứng dụng tích phân tính thể tích',33),
(1,4,'Số phức','S12.34','Khái niệm số phức',34),
(1,4,'Số phức','S12.35','Các phép toán về số phức',35),
(1,4,'Số phức','S12.36','Bài toán tìm số phức',36),
(1,4,'Số phức','S12.37','Công thức Moivre',37),
(1,4,'Số phức','S12.38','Giải phương trình số phức',38),
(1,5,'Khối đa diện','H12.01','Khái niệm và phân loại',01),
(1,5,'Khối đa diện','H12.02','Thể tích khối đa diện',02),
(1,5,'Khối đa diện','H12.03','Bài toán về tứ diện đều',03),
(1,5,'Khối đa diện','H12.04','Bài toán về chóp tam giác, chóp tứ giác',04),
(1,5,'Khối đa diện','H12.05','Bài toán về hình lăng trụ',05),
(1,5,'Khối đa diện','H12.06','Bài toán về hình hộp',06),
(1,6,'Mặt tròn xoay','H12.07','Mặt cầu',07),
(1,6,'Mặt tròn xoay','H12.08','Mặt nón',08),
(1,7,'Phương pháp toạ độ trong không gian','H12.09','Vecto',09),
(1,7,'Phương pháp toạ độ trong không gian','H12.10','Tích có hướng và ứng dụng',10),
(1,7,'Phương pháp toạ độ trong không gian','H12.11','Phương trình đường thẳng',11),
(1,7,'Phương pháp toạ độ trong không gian','H12.12','Vị trí tương đối của hai đường thẳng',12),
(1,7,'Phương pháp toạ độ trong không gian','H12.13','Phương trình mặt phẳng',13),
(1,7,'Phương pháp toạ độ trong không gian','H12.14','Vị trí tương đối của hai mặt phẳng',14),
(1,7,'Phương pháp toạ độ trong không gian','H12.15','Vị trí tương đối của đường thẳng và mặt phẳng',15),
(1,7,'Phương pháp toạ độ trong không gian','H12.16','Mặt cầu',16),
(1,7,'Phương pháp toạ độ trong không gian','H12.17','Mặt cầu và đường thẳng',17),
(1,7,'Phương pháp toạ độ trong không gian','H12.18','Mặt cầu và mặt phẳng',18),
(1,7,'Phương pháp toạ độ trong không gian','H12.19','Khoảng cách',19),
(1,7,'Phương pháp toạ độ trong không gian','H12.20','Ứng dụng tọa độ trong giải toán',20)
;

INSERT INTO edu_ka_io.edu_chapter(subject_id,chapter_no,chapter_name, code_unit, name_unit,order_unit)
VALUES
(3,1,'Hàm số lượng giác và phương trình lượng giác','S11.01','Hàm số lượng giác',01),
(3,1,'Hàm số lượng giác và phương trình lượng giác','S11.02','Phương trình lượng giác cơ bản',02),
(3,1,'Hàm số lượng giác và phương trình lượng giác','S11.03','Phương trình lượng giác asinx+bcosx=c',03),
(3,2,'Tố hợp và xác suất','S11.04','Phương trình quy về bậc hai với hàm số lượng giác',04),
(3,2,'Tố hợp và xác suất','S11.05','Phương trình đẳng cấp',05),
(3,2,'Tố hợp và xác suất','S11.06','Phương trình đối xứng',06),
(3,2,'Tố hợp và xác suất','S11.07','Phương trình lượng giác không mẫu mực',07),
(3,2,'Tố hợp và xác suất','S11.08','Ứng dụng giải PTLG tìm min, max',08),
(3,3,'Tố hợp và xác suất','S11.09','Quy tắc đếm',09),
(3,3,'Tố hợp và xác suất','S11.10','Tổ hợp, chỉnh hợp, hoán vị',10),
(3,3,'Tố hợp và xác suất','S11.11','Phương trình, bất phương trình tổ hợp',11),
(3,3,'Tố hợp và xác suất','S11.12','Nhị thức Newton',12),
(3,3,'Tố hợp và xác suất','S11.13','Xác suất',13),
(3,4,'Dãy số, cấp số cộng, cấp số nhân','S11.14','Phương pháp quy nạp',14),
(3,4,'Dãy số, cấp số cộng, cấp số nhân','S11.15','Dãy số',15),
(3,4,'Dãy số, cấp số cộng, cấp số nhân','S11.16','Cấp số cộng',16),
(3,4,'Dãy số, cấp số cộng, cấp số nhân','S11.17','Cấp số nhân',17),
(3,4,'Dãy số, cấp số cộng, cấp số nhân','S11.18','Dãy số nâng cao',18),
(3,5,'Giới hạn','S11.19','Giới hạn của dãy số',19),
(3,5,'Giới hạn','S11.20','Giới hạn của hàm số dạng 0/0',20),
(3,5,'Giới hạn','S11.21','Giới hạn của hàm số dạng vô cực/vô cực',21),
(3,5,'Giới hạn','S11.22','Hàm số liên tục',22),
(3,5,'Giới hạn','S11.23','Ứng dụng của hàm số liên tục',23),
(3,6,'Đạo hàm','S11.24','Khái niệm đạo hàm',24),
(3,6,'Đạo hàm','S11.25','Quy tắc tính đạo hàm',25),
(3,6,'Đạo hàm','S11.26','Đạo hàm hàm số lượng giác',26),
(3,6,'Đạo hàm','S11.27','Vi phân',27),
(3,6,'Đạo hàm','S11.28','Đạo hàm cấp cao',28),
(3,7,'Phép dời hình và phép đông dạng trên mặt phẳng','H11.01','Phép tịnh tiến',01),
(3,7,'Phép dời hình và phép đông dạng trên mặt phẳng','H11.02','Phép vị tự',02),
(3,7,'Phép dời hình và phép đông dạng trên mặt phẳng','H11.03','Phép đồng dạng',03),
(3,7,'Phép dời hình và phép đông dạng trên mặt phẳng','H11.04','Phép quay',04),
(3,8,'Đường thẳng và mặt phẳng trong không gian.Quan hệ song song','H11.05','Đường thẳng và mặt phẳng',05),
(3,8,'Đường thẳng và mặt phẳng trong không gian.Quan hệ song song','H11.06','Hai đường thẳng chéo nhau',06),
(3,8,'Đường thẳng và mặt phẳng trong không gian.Quan hệ song song','H11.07','Hai đường thẳng song song',07),
(3,8,'Đường thẳng và mặt phẳng trong không gian.Quan hệ song song','H11.08','Hai mặt phẳng song song',08),
(3,8,'Đường thẳng và mặt phẳng trong không gian.Quan hệ song song','H11.09','Phép chiếu song song',09),
(3,9,'Véc tơ trong không gian. Quan hệ vuông góc trong không gian','H11.10','Vecto',10),
(3,9,'Véc tơ trong không gian. Quan hệ vuông góc trong không gian','H11.11','Hai đường thẳng vuông góc',11),
(3,9,'Véc tơ trong không gian. Quan hệ vuông góc trong không gian','H11.12','Đường thẳng vuông góc với mặt phẳng',12),
(3,9,'Véc tơ trong không gian. Quan hệ vuông góc trong không gian','H11.13','Hai mặt phẳng vuông góc',13),
(3,9,'Véc tơ trong không gian. Quan hệ vuông góc trong không gian','H11.14','Khoảng cách',14)
;

INSERT INTO edu_ka_io.edu_chapter(subject_id,chapter_no,chapter_name, code_unit, name_unit,order_unit)
VALUES
(5,1,'Mệnh đề - Tập hợp','S10.01','Mệnh đề',01),
(5,1,'Mệnh đề - Tập hợp','S10.02','Tập hợp',02),
(5,1,'Mệnh đề - Tập hợp','S10.03','Số gần đúng, sai số',03),
(5,2,'Hàm số bậc nhất và hàm số bậc 2','S10.04','Khái niệm hàm số',04),
(5,2,'Hàm số bậc nhất và hàm số bậc 3','S10.05','Hàm số bậc nhất',05),
(5,2,'Hàm số bậc nhất và hàm số bậc 4','S10.06','Hàm số bậc 2',06),
(5,3,'Phương trình và Hệ phương trình','S10.07','Khái niệm về phương trình',07),
(5,3,'Phương trình và Hệ phương trình','S10.08','Phương trình bậc nhất',08),
(5,3,'Phương trình và Hệ phương trình','S10.09','Phương trình bậc hai',09),
(5,3,'Phương trình và Hệ phương trình','S10.10','Định lý Viet',10),
(5,3,'Phương trình và Hệ phương trình','S10.11','Phương trình chứa dấu giá trị tuyệt đối',11),
(5,3,'Phương trình và Hệ phương trình','S10.12','Phương trình chứa ẩn ở mẫu',12),
(5,3,'Phương trình và Hệ phương trình','S10.13','Phương trình vô tỷ',13),
(5,3,'Phương trình và Hệ phương trình','S10.14','Phương trình quy về bậc 2',14),
(5,3,'Phương trình và Hệ phương trình','S10.15','Hệ phương trình bậc nhất hai ẩn',15),
(5,3,'Phương trình và Hệ phương trình','S10.16','Hệ phương trình bậc cao hai ẩn',16),
(5,4,'Bất đẳng thức và bất phương trình','S10.17','Bất đẳng thức',17),
(5,4,'Bất đẳng thức và bất phương trình','S10.18','Bất đẳng thức Cô-si',18),
(5,4,'Bất đẳng thức và bất phương trình','S10.19','Bất đẳng thức Bunhia',19),
(5,4,'Bất đẳng thức và bất phương trình','S10.20','Một số phương pháp chứng minh BĐT thường gặp',20),
(5,4,'Bất đẳng thức và bất phương trình','S10.21','Bất phương trình',21),
(5,4,'Bất đẳng thức và bất phương trình','S10.22','Bất phương trình bậc nhất',22),
(5,4,'Bất đẳng thức và bất phương trình','S10.23','Hệ bất phương trình',23),
(5,4,'Bất đẳng thức và bất phương trình','S10.24','Bất phương trình bậc hai',24),
(5,4,'Bất đẳng thức và bất phương trình','S10.25','Bất phương trình vô tỷ',25),
(5,5,'Thống kê','S10.26','Các đại lượng của thống kê',26),
(5,5,'Thống kê','S10.27','Biểu đồ',27),
(5,6,'Góc – Cung lượng giác – Công thức lượng giác','S10.28','Góc đặc biệt',28),
(5,6,'Góc – Cung lượng giác – Công thức lượng giác','S10.29','Các cung lượng giác liên quan nhau',29),
(5,6,'Góc – Cung lượng giác – Công thức lượng giác','S10.30','Công thức cộng',30),
(5,6,'Góc – Cung lượng giác – Công thức lượng giác','S10.31','Công thức nhân',31),
(5,6,'Góc – Cung lượng giác – Công thức lượng giác','S10.32','Công thức biến đổi',32),
(5,6,'Góc – Cung lượng giác – Công thức lượng giác','S10.33','Tính giá trị lượng giác của 1 góc',33),
(5,6,'Góc – Cung lượng giác – Công thức lượng giác','S10.34','Rút gọn biểu thức lượng giác',34),
(5,7,'Vecto','H10.01','Vecto - Ứng dụng',01),
(5,7,'Vecto','H10.02','Tọa độ',02),
(5,8,'Tích vô hướng của hai vecto và ứng dụng','H10.03','Tích vô hướng',03),
(5,8,'Tích vô hướng của hai vecto và ứng dụng','H10.04','Hệ thức lượng',04),
(5,8,'Tích vô hướng của hai vecto và ứng dụng','H10.05','Tích có hướng (Nâng cao)',05),
(5,9,'Phương pháp toạ độ trong mặt phẳng','H10.06','Phương trình đường thẳng',06),
(5,9,'Phương pháp toạ độ trong mặt phẳng','H10.07','Vị trí tương đối của hai đường thẳng',07),
(5,9,'Phương pháp toạ độ trong mặt phẳng','H10.08','Góc, khoảng cách',08),
(5,9,'Phương pháp toạ độ trong mặt phẳng','H10.09','Phương trình đường tròn',09),
(5,9,'Phương pháp toạ độ trong mặt phẳng','H10.10','Vị trí tương đối của đường thẳng và đường tròn',10),
(5,9,'Phương pháp toạ độ trong mặt phẳng','H10.11','Vị trí tương đối của hai đường tròn',11),
(5,9,'Phương pháp toạ độ trong mặt phẳng','H10.12','Tiếp tuyến của đường tròn',12)
;

INSERT INTO edu_ka_io.edu_chapter(subject_id,chapter_no,chapter_name, code_unit, name_unit,order_unit)
VALUES
(2,1,' DAO ĐỘNG CƠ','L12.01','Đại Cương về dao động điều hòa',01),
(2,1,' DAO ĐỘNG CƠ','L12.02','Các Dạng Toán Cơ Bản',02),
(2,1,' DAO ĐỘNG CƠ','L12.03','Con Lắc Lò Xo',03),
(2,1,' DAO ĐỘNG CƠ','L12.04','Con Lắc Đơn',04),
(2,1,' DAO ĐỘNG CƠ','L12.05','Tổng Hợp hai dao động',05),
(2,1,' DAO ĐỘNG CƠ','L12.06','Các Dạng Toán Nâng Cao',06),
(2,1,' DAO ĐỘNG CƠ','L12.07','Dao Động Tắt Dần',07),
(2,1,' DAO ĐỘNG CƠ','L12.08','Bài Toán Đồ Thị',08),
(2,2,'SÓNG CƠ VÀ SÓNG ÂM','L12.10','Đại Cương Sóng cơ.',10),
(2,2,'SÓNG CƠ VÀ SÓNG ÂM','L12.11','Giao thoa sóng cơ',11),
(2,2,'SÓNG CƠ VÀ SÓNG ÂM','L12.12','Sóng Dừng ',12),
(2,2,'SÓNG CƠ VÀ SÓNG ÂM','L12.13','Sóng Âm  ',13),
(2,2,'SÓNG CƠ VÀ SÓNG ÂM','L12.14','Bài Toán Đồ Thị',14),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.16','Cách tạo ra dòng điện xoay chiều ',16),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.17','Mạch điện chứa một phần tử (R,L,C)',17),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.18','Mạch điện chỉ có R,L,C mắc nối tiếp.',18),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.19','Bài Toán về độ lệch pha',19),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.20','Công suất . Cộng Hưởng Điện',20),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.21','Giải Toán bằng Véc tơ',21),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.22','Bài toán hộp kín (Hộp đen)',22),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.23','Bài Toán Cực Trị',23),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.24','Máy biến áp . Truyền tải điện năng ',24),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.25','Máy Phát Điện và Động cơ điện',25),
(2,3,'DÒNG ĐIỆN XOAY CHIỀU','L12.26','Bài Toán Đồ Thị',26),
(2,4,'DAO ĐỘNG ĐIỆN VÀ SÓNG ĐIỆN TỪ','L12.28','Mạch dao động ',28),
(2,4,'DAO ĐỘNG ĐIỆN VÀ SÓNG ĐIỆN TỪ','L12.29','Sóng điện từ',29),
(2,4,'DAO ĐỘNG ĐIỆN VÀ SÓNG ĐIỆN TỪ','L12.30','Sự Thu- Phát sóng điện từ',30),
(2,4,'DAO ĐỘNG ĐIỆN VÀ SÓNG ĐIỆN TỪ','L12.31','Bài Toán Đồ Thị',31),
(2,5,'SÓNG ÁNH SÁNG','L12.33','Tán sắc ánh sáng',33),
(2,5,'SÓNG ÁNH SÁNG','L12.34','Giao thoa ánh sáng',34),
(2,5,'SÓNG ÁNH SÁNG','L12.35','Quang Phổ và Các loại Tia',35),
(2,6,' LƯỢNG TỬ ÁNH SÁNG','L12.36','Hiện tượng quang điện ngoài ',36),
(2,6,' LƯỢNG TỬ ÁNH SÁNG','L12.37','Hiện Tượng Quang điện trong',37),
(2,6,' LƯỢNG TỬ ÁNH SÁNG','L12.38','Hiện tượng quang - Phát quang',38),
(2,6,' LƯỢNG TỬ ÁNH SÁNG','L12.39','Laze',39),
(2,6,' LƯỢNG TỬ ÁNH SÁNG','L12.40','Mẫu Nguyên tử Bo (Bohr)',40),
(2,7,'HẠT NHÂN NGUYÊN TỬ','L12.41','Cấu tạo hạt nhân nguyên tử',41),
(2,7,'HẠT NHÂN NGUYÊN TỬ','L12.42','Phản Ứng Hạt Nhân',42),
(2,7,'HẠT NHÂN NGUYÊN TỬ','L12.43','Bài Toán Phóng Xạ',43),
(2,7,'HẠT NHÂN NGUYÊN TỬ','L12.44','Phản ứng Phân Hạch . Nhiệt Hạch',44),
(2,8,'Nâng Cao','L12.45','Thuyết Tương Đối Hẹp Anhxtanh',45),
(2,8,'Nâng Cao','L12.46','Từ vi mô đến Vĩ mô',46)
;

INSERT INTO edu_ka_io.edu_chapter(subject_id,chapter_no,chapter_name, code_unit, name_unit,order_unit)
VALUES
  (4,1,'ĐIỆN TÍCH - ĐIỆN TRƯỜNG','L11.01','Điện tích. Định luật Cu - lông.',01),
  (4,1,'ĐIỆN TÍCH - ĐIỆN TRƯỜNG','L11.02','Thuyết electron. Định luật bảo toàn điện tích.',02),
  (4,1,'ĐIỆN TÍCH - ĐIỆN TRƯỜNG','L11.03','Điện trường ',03),
  (4,1,'ĐIỆN TÍCH - ĐIỆN TRƯỜNG','L11.04','Công của lực điện.',04),
  (4,1,'ĐIỆN TÍCH - ĐIỆN TRƯỜNG','L11.05','Điện thế. Hiệu điện thế.',05),
  (4,1,'ĐIỆN TÍCH - ĐIỆN TRƯỜNG','L11.06','Tụ điện.',06),
  (4,2,'DÒNG ĐIỆN KHÔNG ĐỔI ','L11.07','Dòng điện không đổi. Nguồn điện.',07),
  (4,2,'DÒNG ĐIỆN KHÔNG ĐỔI ','L11.08','Điện năng – công suất điện.',08),
  (4,2,'DÒNG ĐIỆN KHÔNG ĐỔI ','L11.09','Định luật Ôm đối với toàn mạch',09),
  (4,2,'DÒNG ĐIỆN KHÔNG ĐỔI ','L11.10','Đoạn mạch chứa nguồn điện . Ghép nguồn',10),
  (4,2,'DÒNG ĐIỆN KHÔNG ĐỔI ','L11.11','Phương Pháp Giải Toán Mạch Điện',11),
  (4,3,'DÒNG ĐIỆN TRONG CÁC MÔI TRƯỜNG.','L11.12','Dòng điện trong kim loại',12),
  (4,3,'DÒNG ĐIỆN TRONG CÁC MÔI TRƯỜNG.','L11.13','Dòng điện trong chất điện phân',13),
  (4,3,'DÒNG ĐIỆN TRONG CÁC MÔI TRƯỜNG.','L11.14','Dòng điện trong các môi trường khác ',14),
  (4,4,'TỪ TRƯỜNG.','L11.15','Từ trường. ',15),
  (4,4,'TỪ TRƯỜNG.','L11.16','Lực từ. Cảm ứng điện từ',16),
  (4,4,'TỪ TRƯỜNG.','L11.17','Từ Trường Của dòng ',17),
  (4,4,'TỪ TRƯỜNG.','L11.18','Lực Lo-ren-xơ',18),
  (4,5,'CẢM ỨNG ĐIỆN TỪ.','L11.19','Từ thông',19),
  (4,5,'CẢM ỨNG ĐIỆN TỪ.','L11.20','Suất điện động cảm ứng',20),
  (4,5,'CẢM ỨNG ĐIỆN TỪ.','L11.21','Tự cảm',21),
  (4,6,'KHÚC XẠ ÁNH SÁNG ','L11.22','Khúc xạ ánh sáng',22),
  (4,6,'KHÚC XẠ ÁNH SÁNG ','L11.23','Phản xạ toàn phần.',23),
  (4,7,'MẮT. CÁC DỤNG CỤ QUANG.','L11.24','Lăng kính',24),
  (4,7,'MẮT. CÁC DỤNG CỤ QUANG.','L11.25','Thấu Kính',25),
  (4,7,'MẮT. CÁC DỤNG CỤ QUANG.','L11.26','Hệ Thấu Kính',26),
  (4,7,'MẮT. CÁC DỤNG CỤ QUANG.','L11.27','Mắt',27),
  (4,7,'MẮT. CÁC DỤNG CỤ QUANG.','L11.28','Kính lúp',28),
  (4,7,'MẮT. CÁC DỤNG CỤ QUANG.','L11.29','Kính hiển vi',29),
  (4,7,'MẮT. CÁC DỤNG CỤ QUANG.','L11.30','Kính thiên văn',30);

INSERT INTO edu_ka_io.edu_chapter(subject_id,chapter_no,chapter_name, code_unit, name_unit,order_unit)
VALUES
  (6,1,'ĐỘNG HỌC CHẤT ĐIỂM','L10.01','Chuyển  động  cơ',01),
  (6,1,'ĐỘNG HỌC CHẤT ĐIỂM','L10.02','Chuyển động thẳng đều.',02),
  (6,1,'ĐỘNG HỌC CHẤT ĐIỂM','L10.03','Chuyển  động  thẳng  biến  đổi  đều',03),
  (6,1,'ĐỘNG HỌC CHẤT ĐIỂM','L10.04','Sự rơi tự do.',04),
  (6,1,'ĐỘNG HỌC CHẤT ĐIỂM','L10.05','Chuyển  động  tròn  đều',05),
  (6,1,'ĐỘNG HỌC CHẤT ĐIỂM','L10.06','Công thức cộng vận tốc',06),
  (6,2,'ĐỘNG LỰC HỌC CHẤT ĐIỂM','L10.07','Tổng hợp và phân tích lực. ',07),
  (6,2,'ĐỘNG LỰC HỌC CHẤT ĐIỂM','L10.08','Ba định luật Niu-tơn.',08),
  (6,2,'ĐỘNG LỰC HỌC CHẤT ĐIỂM','L10.09','Định luật vạn vật hấp dẫn',09),
  (6,2,'ĐỘNG LỰC HỌC CHẤT ĐIỂM','L10.10','Lực đàn hồi của lò xo. Định luật Húc.',10),
  (6,2,'ĐỘNG LỰC HỌC CHẤT ĐIỂM','L10.11','Lực ma sát',11),
  (6,2,'ĐỘNG LỰC HỌC CHẤT ĐIỂM','L10.12','Lực hướng tâm',12),
  (6,2,'ĐỘNG LỰC HỌC CHẤT ĐIỂM','L10.13','Phương Pháp Giải Bài Toán Động Lực Học',13),
  (6,2,'ĐỘNG LỰC HỌC CHẤT ĐIỂM','L10.14','Ném xiên - Ném ngang',14),
  (6,3,'CÂN BẰNG VÀ CHUYỂN ĐỘNG CỦA VẬT RẮN','L10.15','Cân bằng của lực đồng quy',15),
  (6,3,'CÂN BẰNG VÀ CHUYỂN ĐỘNG CỦA VẬT RẮN','L10.16','Mô men Lực',16),
  (6,3,'CÂN BẰNG VÀ CHUYỂN ĐỘNG CỦA VẬT RẮN','L10.17','Cân bằng của lực Song Song',17),
  (6,3,'CÂN BẰNG VÀ CHUYỂN ĐỘNG CỦA VẬT RẮN','L10.18','Các Dạng cân bằng',18),
  (6,3,'CÂN BẰNG VÀ CHUYỂN ĐỘNG CỦA VẬT RẮN','L10.19','Chuyển động của vật rắn',19),
  (6,3,'CÂN BẰNG VÀ CHUYỂN ĐỘNG CỦA VẬT RẮN','L10.20','Ngẫu lực.',20),
  (6,4,'CÁC ĐỊNH LUẬT BẢO TOÀN','L10.21','Định luật bảo toàn Động Lượng',21),
  (6,4,'CÁC ĐỊNH LUẬT BẢO TOÀN','L10.22','Công và công suất.',22),
  (6,4,'CÁC ĐỊNH LUẬT BẢO TOÀN','L10.23','Động năng',23),
  (6,4,'CÁC ĐỊNH LUẬT BẢO TOÀN','L10.24','Thế năng.',24),
  (6,4,'CÁC ĐỊNH LUẬT BẢO TOÀN','L10.25','Định luật bảo toàn cơ năng',25),
  (6,5,'CHẤT KHÍ','L10.26','Thuyết động học phân tử chất khí.',26),
  (6,5,'CHẤT KHÍ','L10.27','Định luật Bôi-lơ - Ma-ri-ôt',27),
  (6,5,'CHẤT KHÍ','L10.28','Định luật Sác-lơ.',28),
  (6,5,'CHẤT KHÍ','L10.29','Phương trình trạng thái của khí lý tưởng.',29),
  (6,6,'CƠ SỞ CỦA NHIỆT ĐỘNG LỰC HỌC','L10.30','Nội năng và sự biến đổi nội năng.',30),
  (6,6,'CƠ SỞ CỦA NHIỆT ĐỘNG LỰC HỌC','L10.31','Nguyên  lý của nhiệt  động  lực  học',31),
  (6,7,'CHẤT RẮN VÀ CHẤT LỎNG, SỰ CHUYỂN THỂ','L10.32','Chất Rắn kết tinh - Vô định hình',32),
  (6,7,'CHẤT RẮN VÀ CHẤT LỎNG, SỰ CHUYỂN THỂ','L10.33','Sự nở vì nhiệt của vật rắn.',33),
  (6,7,'CHẤT RẮN VÀ CHẤT LỎNG, SỰ CHUYỂN THỂ','L10.34','Hiện tượng bề mặt của chất lỏng.',34),
  (6,7,'CHẤT RẮN VÀ CHẤT LỎNG, SỰ CHUYỂN THỂ','L10.35','Sự chuyển thể của các chất.',35),
  (6,7,'CHẤT RẮN VÀ CHẤT LỎNG, SỰ CHUYỂN THỂ','L10.36','Độ ẩm của không khí.',36);

INSERT INTO edu_ka_io.examination_papper
(exam_name,code_exam,author,year_exam,class_id,subject_id,time_expire,num_of_quiz,level,description)
VALUES
('KỲ THI TRUNG HỌC PHỔ THÔNG QUỐC GIA NĂM 2017',
'MS19','BỘ GIÁO DỤC VÀ ĐÀO TẠO',2017,12,1,90,50,1,
'Đề thi thử nghiệm lần 2');

INSERT INTO edu_ka_io.edu_exam_quiz
(num_of_quest,question,answera, answerb, answerc, answerd, correct_answer, detail_answer, explain_answer, level,solution)
VALUES (1,'Đường thẳng nào sau đây là tiệm cận đứng của đồ thị hàm số ',
        'x=1','y=-1','y=2','x=-1','D','Không có lời giải chi tiết',
        'Không có giải thích câu trả lời',1,
        '(x+1 = 0 -> x = -1)')